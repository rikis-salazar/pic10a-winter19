# PIC 10A: Introduction to programming (winter 2019)

This is to be considered the class website. Here you will find handouts, slides,
and code related to the concepts we discuss during lecture.

## Sections

*   [The class syllabus][syllabus] ([`.pdf` version][syllabus-pdf])
*   [Lessons: pdf files & Google slide presentations][lessons]
*   [Weekly summaries][weekly]
*   [Class assignments][hw]
*   [Practice code (by lesson number)][code]
*   [Horstmann Graphics][hand2]
*   [Walk troughs, links & handouts][handouts]: includes practice material for
    midterms.

> At some point during this quarter, the last ~~two sections~~ section above
> will no longer be hosted at CCLE. If things go as planned, the transition
> should be smooth and you might not even notice the changes.

[syllabus]: syllabus/
[syllabus-pdf]: syllabus/syllabus.pdf
[handouts]: https://ccle.ucla.edu/course/view/19W-COMPTNG10A-4?section=2
[lessons]: lessons/
[code]: lessons/code
[hw]: assignments/
[weekly]: summaries/
[hand2]: handouts/


## Additional resources

Here are other sites associated to our course:

*   [The CCLE class website][CCLE]: this will be used only for class
    announcements, and homework submissions.
*   [Discussion section material (examples, code, etc.)][ds]: Your TA might
    decide to post discussion material for the beneffit of the class as a whole.

[CCLE]: https://ccle.ucla.edu/course/view/19W-COMPTNG10A-4
[ds]: https://ccle.ucla.edu/course/view/19W-COMPTNG10A-4?section=6
