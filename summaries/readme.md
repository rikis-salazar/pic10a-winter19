# Weekly summaries

Here is a brief description of the topics we have discussed during this quarter.

## Week 1

*   Monday
    -   Syllabus overview
    -   Hello World (line by line)
*   Wednesday
    -   Incomplete Hello World programs: _what happens if a line of code is
        missing._
    -   Errors: _compile_ vs _logic_ 
    -   Indentation & comments
    -   Special output characters
*   Friday
    -   Projects in Visual Studio 2017: _empty projects, common mistakes, and
        solution folder hierarchy structure._
    -   Variables
    -   Data types: `int`, `double`, `char`, `bool`, etc.

## Week 2

*   Monday
    -   Variables: _proper way to name them._
    -   Constants
    -   Console input: `std::cin`
    -   Buffering
    -   Operators  
        *   _Arithmetic:_ `+`, `-`, `*`, `/`.  
        *   _Assignment & shortcut-like:_ `=`, `+=`, `-=`, `*=`, `/=`.  
        *   _Other:_ `%` (modulo), `++` (increment), `--` (decrement), `-`
            (unary).  
*   Wednesday
    -   Casting
    -   Rounding (without using `std::round`)
    -   Classes & objects (_vs_ primitive types & variables)
    -   The `std::string` class (simple examples)
    -   `getline` _vs_ multiple `cin` statements
*   Friday
    -   String concatenation (operator `+`)
    -   The `.length()` member function.
    -   The `.substr( start_index, length )` member function.
    -   Formatting output: the `fixed`, and `setprecision(the_precision)`
        functions. 
    -   Formatting output: the `setw(the_width)` function.
    -   Formatting output: the `left`, and `right` modifiers.

## Week 3

*   Monday (_MLK Jr holiday_)
*   Wednesday
    -   The `.insert( start_index, str_to_insert )` string member function.
    -   The `.erase( start_index, length )` string member function.
    -   The `.find( str_to_search, start_index )` string member function.
    -   Member functions of the _non-standard_ `Time` class[^one].
    -   Discussing possible member functions of a user-defined `Point2d` class.
*   Friday
    -   The `Point` class
    -   The `Circle` class
    -   The `Line` class
    -   Graphics projects in Visual Studio 2017 (Windows only)

[^one]: This class is _declared_ and _defined_ in special files provided by the
  author of your textbook. Since it is not _part of_ C++, I will not ask
  you about its member functions, in any of the upcoming exams.

## Week 4

*   Monday
    -   Graphics projects via an Xquartz terminal (Mac OS X only).
    -   Graphics projects in Visual Studio Code (Mac OS X).
    -   The `if` statement
    -   The `if/else if` statement
    -   The `if/else if/else` statement
*   Wednesday
    -   Midterm preparation: [this website section][here], [study guide (CCLE
        announcements)][guide], and [practice exams][ccle-resources].
    -   De Morgan's laws
    -   Input validation via conversion of `cin` to a boolean value.
    -   Other conversions to boolean values.
    -   The `while` statement
*   Friday
    -   Using the debugger: setting _breakpoints_, using the _step over_
        and _continue_ buttons.
    -   Midterm (Q & A session).

## Week 5

*   Monday
    -   Midterm
*   Wednesday
    -   Graphics projects in Visual Studio Code (Mac OS X).
    -   The `do-while` statement
    -   The `for` statement
*   Friday
    -   Graphics projects in Xcode (Mac OS X).
    -   The `switch` statement

## Week 6

*   Monday
    -   Functions: _syntax_, _usage_, and examples.
    -   Case study (_factorization_): using _pseudo-code_ and _stubs_.
*   Wednesday
    -   Case study (_factorization_): continuation of previous lecture.
    -   Functions: _value_ parameters.
*   Friday
    -   Using the debugger: using the _step-into_, and _step-out_ buttons.
    -   Functions: _reference_ parameters.
    -   The _scope_ of a variable.
    -   Global variables/constants.

## Week 7

*   Monday (_Presidents' day holiday_)
*   Wednesday 
    -   [_Pseudo_]random numbers: generating numbers with `rand()`. `srand()`.
    -   [_Pseudo_]random numbers: seeding the RNG engine.
    -   Enumerated types.
    -   Casino style games: _craps_, _Las Vegas roulette_.
*   Friday
    -   User defined classes: the _interface_.
    -   User defined classes: the `private` and `public` keywords.
    -   User defined classes: the `const` modifier.
    -   User defined classes: _accessors_ and _mutators_.

## Week 8

*   Monday
    -   Review of key concepts: interface, constructors, member functions and
        member fields.
    -   The `RightTriangle` class: _the game plan_.
*   Wednesday
    -   The `RightTriangle` class: _function overloading (constructors)_.
    -   The `RightTriangle` class: the `const` modifier.
        +   Accesor functions: _getters_.
        +   Mutator functions: _setters_.
        +   Importance of `const` correctness.
*   Friday
    -   The `Product` class: _implicit_ vs _explicit_ parameters.
    -   The `Product` class: operator overloading.
    -   The `Product` class: separate compilation.

## Week 9

*   Monday
    -   The `RightTriangle` class: the final version + separate compilation.
    -   The `std::vector<ItemType>` class: introduction.
*   Wednesday
    -   The `std::vector<ItemType>` class: accessing elements (_e.g.,_ `v[2] =
        3`).
    -   The `std::vector<ItemType>` class: common mistakes.
        +   Off By One Errors (OBOEs)
        +   Stuck in an infinite loop 
    -   The `std::vector<ItemType>` class: vectors as parameters.
    -   The `std::vector<ItemType>` class: vectors as return objects.

---

[here]: ./
[ccle-resources]: https://ccle.ucla.edu/course/view/19W-COMPTNG10A-4?section=2
[guide]: https://ccle.ucla.edu/mod/forum/view.php?id=2182685


[Return to main course website](..)
