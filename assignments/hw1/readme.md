# Coding assignment: The journey to _Acapulco_

| [ ![eso-eso-eso.png](eso-eso-eso.jpg) ][chavo-wiki] |  
|:-----:|  
| Figure 1: _El Chavo del 8_ (the kid who lives at [apartment] number 8) |  

[chavo-wiki]: https://en.wikipedia.org/wiki/El_Chavo_del_Ocho

## The backstory


_El Chavo del 8_ (the 'kid' pictured above) is a very poor boy who lives
somewhere in _Mexico City_. He has always dreamed of traveling to _Acapulco_, a
popular beach resort in the state of _Guerrero_ on the Pacific coast of
_México_, located `377.6 Km` away from where _el chavo_ lives. His neighbor
_Don Ramón_ (who is also poor), has the rare opportunity to borrow a rather old
car and offers to take _el chavo_ to _Acapulco_ with him.

_Alas!_ Disaster strikes along the way and the car breaks down. Since they are
already on their way and far from where they live, they decide to continue the
journey on foot! _El chavo_ takes note of the distance traveled so far, and wants
to know exactly how long it is going to take them to get to _Acapulco_ if they
travel at a speed of `5 Km/h`. Unfortunately, neither _el chavo_ nor _Don Ramón_
are good with math and they have no idea how long the journey will take. The
only thing they managed to do is look at each other and say:

> _Y ahora, ¿quién podrá ayudarnos?_ [Who can help us now?].


## Your assignment

You are to play the role of _El Chapulín Colorado_ [the red grasshopper], a
superhero that appears whenever the phrase _“Y ahora, ¿quién podrá ayudarnos?”_
is said out loud.

Write a program that solves _el chavo_’s dilemma. Assume that the user will
**input the distance traveled** (in _Km_) at the moment the car broke down. The
program should then **display the remaining distance** (in _Km_) and **tell the
user how long will it take to get to the destination**. Next, it should convert
this distance to time, and report this to the user in **days**, **hours**,
**minutes** and the **nearest rounded second**.

Here is an example of how the program should behave if the user enters `100.3`
kilometers.

| ![hw1-example.png](hw1-example.png) |  
|:-----:|  
| Figure 2: _Sample run of my solution._ |  

Your program should use the maximum amount of the largest unit of time, starting
with days and so on. In the example above, the following output would not be
acceptable.

~~~~~
...
Traveling at a speed of 5 Km/h, you will reach it in:
0 days, 0 hours, 0 minutes and about 199656 seconds.
~~~~~

For this assignment you may assume:

*   The user will input a decimal between `0` and `377.6` with **at most 7
    decimals**.
    
    _E.g.,_ users could enter `377`, or `234.0000008`, or `32.5`; but they will
    not enter `-23`, nor `380`, nor `100.12345678`.

The following miscellaneous information might come in handy at some point.

*   _time_ = _distance_ &#8725; _speed_
*   5 _Km/h_ = 5000 _m/h_ = 1.38889 _m/s_
*   1 _Km_ = 1000 _m_
*   1 day = 24 hours = 1440 minutes = 86400 seconds
*   1 hour = 60 minutes = 3600 seconds

> **Note:**
>
> An earlier version of this document stated that 5 _Km/h_ = 1.8889 _m/s_. The
> correct conversion factor is 1.38889.


## Submission

Place your code in a source file labeled `hw1.cpp` (all lowercase). **If your
file is not named exactly like this, your homework will not be graded.** Your
code should contain useful comments as well as your name, the date, and a brief
description of what the program does. Upload your file to the [assignments
section of the CCLE website][ccle-hw]. The file will be automatically collected
at the date and time listed in _"submission status"_ table at the bottom of the
CCLE assignment description corresponding to this project.

[ccle-hw]: https://ccle.ucla.edu/course/view.php?id=67961&section=4on=2


### Grading rubric

Points will be assigned based on the following categories:

| **Category** | **Description** | **Points** |  
|:-----|:----------------------------------------------------------|:---:|  
| **Correctness** | Code compiles, no warnings, reads input, and the output is presented nicely. | 4 |  
| **Time breakdown** | Correctly breaks down the time in days, hours, and minutes. It also rounds off to the nearest second. | 4 |  
| **Form** | Variable names, comments, indentation, magic numbers. | 4 |  
| **Efficiency** | Computations are simple and accurate; the appropriate number of variables is used to perform all needed computations. | 4 |  
| **Readability** | Code is easy to understand and follow; comments improve (as oppose to decrease) the quality of the code. | 4 |  


---

[Return to main course website][PIC]

[PIC]: ../..
