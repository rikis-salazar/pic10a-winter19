# Coding assignment: the trip back to _Ciudad de México_

| [ ![travesia.png](travesia.png) ][travesia] |  
|:-----:|  
| Figure 1: _La travesía de Don Ramón y El Chavo._<br>(_Travesía_ = very long trip) |

[travesia]: https://youtu.be/7jZCVHo_E2Q

## The backstory

As you know, _Don Ramón_ and _el Chavo_ are heading home. As they are about to
buy the tickets for the _guajolotero_ bus, they realize they only have about 6
more days before _el día del amor y la amistad_ (_i.e.,_ Valentine's day).
Figuring they would get to _Mexico City_ with no money at all, and hence unable
to buy things for _Gloria_ and _Pati_ whom they hope would be their Valentines,
they decide to change strategy.

They hitch a ride with a person who claims is going to _Mexico City_. The only
problem is that this person turns out to be a _chofer de paquetería_ [delivery
truck driver], who indeed is heading to _Mexico City_ after making stops at
several other places: including but not limited to _Ixtapa_, _Iguala_,
_Cuernavaca_, _Puebla_, _Teziutlán_, _Poza Rica_, _Salamanca_, _Morelia_ and
_Toluca_. They know it will take a little longer than a day, but since they are
also not very good with geography (apart from _Cuernavaca_, they are clueless
about the locations of those other places), they have no way to know how long it
is going to take, nor how many kilometers they will travel.

If only they had a GPS device, or something that would tell them how far two
cities are; and that would graphically indicate whether they are traveling east
or west...

> _Y ahora, ¿quién podrá ayudarlos?_


## The assignment

You are to implement a basic trip planner. First draw a graphical representation
(simplified map) of the cities listed below. Then let a user choose a series of
destinations in the map. Assuming the user will start the journey at _Acapulco_,
after every click (representing the next destination), you should:

*   draw a line joining the current destination to the next destination;
*   display the distance (in Km) between the current and next destination; and
*   indicate graphically whether the user will be traveling east or west.

For this assignment you may assume that

*   the user will not click outside of the map; and
*   the user will end the trip when a location within 20 km of _Mexico City_ is
    selected as the next destination.

Once the user has selected _Mexico City_ as the next and final destination, you
should report at the bottom of the screen, in a location outside of the map, the
total distance traveled in Kilometers.

Also, since **it is a well-known fact that Cuernavaca (my hometown) is the
center of the universe**, you should display a message indicating how many times
the user passes within 50 Km of the center of the universe. Trips that start
within 50 Km of _Cuernavaca_ should not count as a pass.

Once this process has been completed you should ask the user whether or not s/he
wants to continue, then proceed accordingly.

The locations of the cities that are to be represented are given below:

1.  Cuernavaca `(0,0)`
2.  Ciudad de México `(0,75)`
3.  Acapulco `(-50,-225)`
4.  Ixtapa `(-300,-200)`
5.  Salamanca `(-225,250)`
6.  Morelia `(-250,125)`
7.  Casitas `(300,225)`

You should declare these points as global constants, but notice that you are not
allowed to use any other global variables in your program.

In addition, the formulas below might come in handy at some point:

If $d$ represents the distance between two points $(x_{1},y_{1})$ and
$(x_{2},y_{2})$, then

$$d = \sqrt{ (x_{1}-x_{2})^{2} + (y_{1}-y_{2})^{2} }.$$

Similarly, if $D$ represents the distance between a point $(h,k)$ and a line
segment with ends $(x_{1},y_{1})$ and $(x_{2},y_{2})$, then

$$D = \begin{cases} \sqrt{(h-x_{1})^{2}+(k-y_{1})^{2}},
    & \text{ if } t\le 0 \\
      \sqrt{(h-x_{1}-tx_{2}+tx_{1})^{2}+(k-y_{1}-ty_{2}+ty_{1})^{2}},
    & \text{ if } 0 < t < 1 \\
      \sqrt{(h-x_{2})^{2}+(k-y_{2})^{2}},
    & \text{ if } t\ge 1 \end{cases},$$

where

$$t = \dfrac{(h-x_{1})(x_{2}-x_{1})+(k-y_{1})(y_{2}-y_{1})}
            {(x_{2}-x_{1})^{2}+(y_{2}-y_{1})^{2}}.$$

This latter formula looks complicated because we’re trying to compute the
distance from a point to a line segment, as opposed to the distance to the
infinite line passing through the given points.

Below is my interpretation of central _México_. The screenshot below shows what
initially appears on the screen.

| ![shot1.png](shot1.png) |  
|:-----:|  
| Figure 2: The initial setup. |  

These other screenshot shows an almost direct route.

| ![shot2.png](shot2.png) |  
|:-----:|  
| Figure 3: An almost direct route. |  

And this other one shows a route with several different stops along the way.

| ![shot3.png](shot3.png) |  
|:-----:|  
| Figure 4: A not-so-direct route. |  

The bare minimum your program should show is listed below:

*   The 7 cities listed above with their names;
*   Tick marks every 100 Kilometers and labels every 200 Kilometers.
*   Some graphical representation of the direction of travel (east or west).

    I have chosen to draw a truck facing left or right accordingly. Whatever you
    choose must not be a simple shape like an arrow or a message. The
    representation must include **at least one rectangle** and **at least one
    circle**.


## What is this assignment about?

The purpose of this assignment is that you code your program efficiently **using
functions**. Your program should demonstrate a knowledge and proficiency with
modular programming. I strongly recommend using the following functions:

*   `drawMap` — to draw the basic map of central Mexico on the screen.
*   `drawTruck` — to draw a basic representation of the direction of travel.
*   `connectCities` — to draw a line between two adjacent destinations.
*   `distanceBetweenCities` — to compute the distance between two adjacent
    destinations.
*   `distanceToCuernavaca` (alternatively: `distanceToCenterOfTheUniverse`) — to
    compute the closest distance from a line segment to the center of the map.

The choice of return values and parameters is up to you. You may wish to create
other functions as well. I suggest getting the `drawMap` function working first,
then planning your output around the map. The next step should be to work on the
`drawTruck` function, making sure you can move your truck around and have it
face east or west. As a third step you might want to implement the
`connectCities` function ([this example][sketch] might come in handy). The
functions that compute distances are simple in comparison to the other functions
but you need to make sure the computations are accurate. The final step of
course would be to make sure the individual pieces work together as a whole.

Needless to say that this assignment takes some careful planning. Even though
you will have more time, do not wait until the last minute.

I have taken my version of this trip planner and compiled it to produce an
executable (`.exe`) file. You should be able to download it and run it on one of
the machines in the PIC Lab. It might give you a better idea of what is expected
from your program ([click here to download it][exe]).

[sketch]: ../../lessons/code/07/sketch.cpp
[exe]: https://ccle.ucla.edu/mod/resource/view.php?id=2322161


## Extra credit

The following features of your program could result in extra credit on your
homework score. I strongly suggest you get the basic program to work before
attempting to implement any of these features. Neither I nor the TA’s will give
you any help with the extra credit.

*   (+1 point) Detect when a user clicks outside of the map and kindly reject
    the click.
*   (+1 points) Implement a re-sizable truck. The truck should get smaller as
    the destination gets farther away from _Mexico City_. Similarly the truck
    should get bigger the closer it is to _Mexico City_.
*   (+2 points) For a graphic representation of the truck that points towards
    _Mexico City_. That is, instead of the truck facing east or west, the truck
    should rotate and have the front part of it point towards the final
    destination.


## Submission

Place your code in a source file labeled `hw4.cpp` (all lowercase). If your file
is not named exactly like this, your homework might not be graded. Your code
should **contain useful comments** as well as **your name**, **the date**, and a
**brief description of what the program does**. Upload your file to the
[assignments section of the CCLE website][ccle-hw]. The file will be
automatically collected at the date and time listed in _"submission status"_
table at the bottom of the CCLE assignment description corresponding to this
project.

[ccle-hw]: https://ccle.ucla.edu/course/view.php?id=67961&section=4on=2


## Grading rubric

| **Category** | **Description** | **Points** |  
|:---------------|:------------------------------|:----------:|  
| **Correctness** | No errors. The trip is displayed correctly and the program ends when instructed. | 4 |  
| **Graphics** | Draws map with ticks and labels, records mouse clicks, outputs line segments correctly. | 4 |  
| **Input/Output** | The distances are computed accurately. The number of times a trip passes close to _Cuernavaca_ is correctly reported. | 4 |  
| **Solution** | The code is efficient but easy to follow. Functions are used appropriately. | 4 |  
| **Style** | Variable names, comments, indentation, etc. | 4 |  
| **Total** | | 20 | 


---

[Return to main course website][PIC]

[PIC]: ../..
