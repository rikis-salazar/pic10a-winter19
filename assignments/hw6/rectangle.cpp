#include "rectangle.h"

/*
    Default constructor:
        A unit square with main corner (0,0) rotated 0 degrees.
*/
Rectangle::Rectangle(){
    // -- STATEMENTS --
}


/*
    Constructor:
        A rectangle with main corner 'c', width 'w', height 'h' and rotated 0
        degrees.
*/
Rectangle::Rectangle(Point c, double w, double h){
    // -- STATEMENTS --
}


/*
    Constructor:
        A rectangle with main corner 'c', width 'w', height 'h' and rotated 'a'
        degrees.
*/
Rectangle::Rectangle(Point c, double w, double h, double a){
    // -- STATEMENTS --
}


/*
    Accessor:
        Draws lines from corner to corner to generate a rectangle.
*/
void Rectangle::draw() const {
    // -- STATEMENTS --
    return;
}


/*
    Mutator:
        Moves all corners dx units horizontally and dy units vertically
*/
void Rectangle::move(double dx, double dy){
    // -- STATEMENTS --
    return;
}


/*
    Mutator:
        Resizes the rectangle by a factor 'r'. Should be resized around its
        center.

    E.g:
        Rectangle r1;
        r1.resize(2);
    doubles the lenght and width of r1 while keeping the center of the rectangle
    unchanged.

    Hint:
        Move the rectangle so that its center is at the origin then simply
        resize the corners and move the rectangle back to its original position.
*/
void Rectangle::resize(double r){
    // -- STATEMENTS --
    return;
}


/*
    Mutator:
        Rotates the rectangle 'a' degrees around its center.

    E.g:
        Rectangle r1 = Rectangle(Point(-1.0, 0.5), 2, 1);
        r1.rotate(90);
    results in a rectangle with width = 1 and height = 2 centered at the origin.

    Hint:
        Move the rectangle so that its center is at the origin and rotate all
        corners of the rectangle with the help of the private member function
        rotatePoint(Point&, double) below, then move the rectangle back to its
        original position.

        Do not forget to update the angle!
*/
void Rectangle::rotate(double a){
    // -- STATEMENTS --
    return;
}


/*
    Accesor:
        Returns the 'main' corner of the rectangle.
*/
Point Rectangle::get_corner() const {
    // -- STATEMENTS --
    return Point(0, 0);  // <-- Change when ready
}


/*
    Accessor:
        Returns the width of a rectangle. For our purposes the width is the
        distance from corners 'main' and 'next' of the associated rectangle. You
        may assume that to get to corner 'next' from 'main' you simply move
        forward in the clockwise direction.
*/
double Rectangle::get_width() const {
    // -- STATEMENTS --
    return 1.0;   // <-- Change when ready
}


/*
    Accessor:
        Returns the height of a rectangle. For our purposes the height is the
        distance from corner 'main' to 'previous'. See comments above (in
        `get_width`).
*/
double Rectangle::get_height() const {
    // -- STATEMENTS --
    return 1.0;   // <-- Change when ready
}


/*
    Accessor:
        Returns the angle that the line segment from corner 'main' to corner
        'next' makes with a horizontal line.
*/
double Rectangle::get_angle() const {
    // -- STATEMENTS --
    return 0.0;   // <-- Change when ready
}


/*
    Operators:
        Comparison of Rectangles based on their areas.
*/
bool Rectangle::operator<(Rectangle b) const {
    // -- STATEMENTS --
    return true;   // <-- Change when ready
}

bool Rectangle::operator>(Rectangle b) const {
    // -- STATEMENTS --
    return true;   // <-- Change when ready
}

bool Rectangle::operator<=(Rectangle b) const {
    // -- STATEMENTS --
    return true;   // <-- Change when ready
}

bool Rectangle::operator>=(Rectangle b) const {
    // -- STATEMENTS --
    return true;   // <-- Change when ready
}



/*
    Private member functions:
       These functions are only available to members of the class Rectangle.
       Explicit objects are not needed in order to call them.
*/

// Rotates a point 'a' degrees counterclockwise around the origin
void Rectangle::rotatePoint(Point& p, double a){
    // -- STATEMENTS --
    return;
}

// Returns the distance between two points
double Rectangle::distanceBetween(Point a, Point b) const {
    // -- STATEMENTS --
    return 1.0;   // <-- Change when ready
}

// Returns the area of a rectangle
double Rectangle::get_area() const {
    // -- STATEMENTS --
    return 1.0;   // <-- Change when ready
}

// Returns the center of a rectangle
Point Rectangle::get_center() const {
    // -- STATEMENTS --
    return Point(0, 0);   // <-- Change when ready
}
