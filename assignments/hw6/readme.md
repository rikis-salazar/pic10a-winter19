# Coding assignment: a `Rectangle` class.

| [ ![charlando.png](charlando.png) ][charlando] |  
|:-----:|  
| Figure 1: _El Chapulín Colorado_ and _El Chavo_ discussing `C++` graphics. |

[charlando]: https://www.youtube.com/watch?v=5Y1zprD-bGY


## The backstory

As you know by now, _El Chavo_ is back at his ~~house~~ wood barrel in _La
Vecindad_. He managed to come back just before _el día del amor y la amistad_,
but unlike _Don Ramón_ who managed to save a little amount of money to start his
Casino, _El Chavo_ was not able to save even one _centavo_. To make matters a
little worse, he has a huge crush on _Pati_ (Patricia). Back when he started his
Journey, he thought he would be able to bring _Pati_ something nice, like
_tamarindos_ or _cocadas_; and whereas he actually managed to buy these
hand-crafted delicacies[^one] while in _Acapulco_, on the long way back he was
not able to stave off his hunger and wound up eating each and every single one
of these ~~nutritious pieces of food~~ candies.

So, in lieu of a present for _Pati_, he decides to tell her everything about his
_travesía_. He hopes to explain every little thing that he and _Don Ramón_ went
through on their long trip. In particular he would like to be able to somehow
describe all the beautiful scenery he was able to witness. 

He believes that to better understand the journey, some graphics could help. So
he calls his (by now close) friend _El Chapulín Colorado_ and kindly requests
some programming lessons. Unfortunately though, _El chapulín_ is very busy saving
the world.

> _Y ahora, ¿quién podrá ayudarlo?_

[^one]: Don't believe they are delicacies? Just ask a group of Mexican kids.


## The assignment

Your are to implement the member functions of a `Rectangle` class that works
with your graphics library files. For this assignment I am providing the [main
routine][driver] and the class declaration file [`rectangle.h`][h_file]; your job
is then to write the member function definitions in the corresponding file
[`rectangle.cpp`][cpp_file].

It is important that you do not make any changes to the main routine
([`hw6-driver.cpp`][driver]), nor the interface file ([`rectangle.h`][h_file]);
as only the definition file ([`rectangle.cpp`][cpp_file] will be collected.

The functions you need to define are declared in the interface available inside
the [`rectangle.h`][h_file] file. The three files provided here do compile
together, but they do not produce meaningful graphics. When your project is
complete, they should produce something that looks more or less like the pictures
below.

| ![rectangle-initial.png](rectangle-initial.png) |  
|:-----:|  
| Figure 2: _Initial state of the program._ |  

| ![rectangle-final.png](rectangle-final.png) |  
|:-----:|  
| Figure 3: _State of the program after 11 mouse clicks._ |  

To get a better idea of what to expect, you can use [this program][binary],
which corresponds to my implementation of the member functions of the
`Rectangle` class. 

> **Note:** the program was compiled at the PIC lab, if you have a windows
> computer it might, or might not, work in your device. It definitely **does
> not** work in _Mac OS_, nor _Linux_ devices.

[driver]: hw6-driver.cpp
[h_file]: rectangle.h
[cpp_file]: rectangle.cpp
[binary]: https://ccle.ucla.edu/mod/resource/view.php?id=2352266


The following formulas might come in handy at some point. Let $ABCD$ denote a
rectangle with corner points $A=(x_{A},y_{A})$, $B=(x_{B},y_{B})$,
$C=(x_{C},y_{C})$, and $D=(x_{D},y_{D})$.

The center of mass $CM = (\tilde{x}_{M},\tilde{y}_{M})$ of the rectangle $ABCD$
is given by

$$\begin{align*}
      \tilde{x}_{M} & = \frac{ x_{A} + x_{B} + x_{C} + x_{D} }{4}, \\
      \tilde{y}_{M} & = \frac{ y_{A} + y_{B} + y_{C} + y_{D} }{4}.  
  \end{align*}$$

To rotate a point $P$ with coordinates $(x,y)$ around the origin $\theta$
degrees counterclockwise, use the set of formulas

$$\begin{align*}
      x_{\theta} & = x \cos\theta - y \sin\theta, \\
      y_{\theta} & = x \sin\theta + y \cos\theta.  
  \end{align*}$$

> **Note:** here the angle $\theta$ is assumed to be measured in _radians_ as
> opposed to _degrees_. 

You may want to write first the member functions that are easy to figure out. I
suggest saving the functions `resize(...)` and `rotate(...)` for last. Your TA will
give you hints during discussion on how to rotate and scale the rectangles.


## Submission

Place your code in a source file labeled `rectangle.cpp` (all lowercase). If
your file is not named exactly like this, your homework might not be graded.
Your code should **contain useful comments** as well as **your name**, **the
date**, and a **brief description of what the program does**. Upload your file
to the [assignments section of the CCLE website][ccle-hw]. The file will be
automatically collected at the date and time listed in the _"submission status"_
table at the bottom of the CCLE assignment description corresponding to this
project.

[ccle-hw]: https://ccle.ucla.edu/course/view.php?id=67961&section=4on=2

## Grading rubric

| **Category** | **Description** | **Points** |  
|:-------------|:----------------------------------------------|:------:|  
| **Correctness** | The programs compiles (_i.e.,_ no errors were introduced) and meaningful info is displayed to the viewing window. | 4 |  
| **Accessors** | The accessor functions are properly defined. _E.g.:_ The area, and center of the rectangles are correctly computed; and the corner and angle are correctly reported. | 4 |  
| **Mutators** | The geometric transformations work as expected (_e.g.,_ rotate, move, resize, etc.). | 4 |  
| **Solution** | The code is efficient but easy to follow. No additional functions are created and the ones already there are used appropriately. | 4 |  
| **Style** | Variable names, comments, indentation, etc. | 4 |  
| **Total** | | 20 |  


---

[Return to main course website][PIC]

[PIC]: ../..
