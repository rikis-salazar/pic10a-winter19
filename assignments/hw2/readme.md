# Coding assignment: Some _conversion_ issues

| [ ![chavo-ramon-acapulco.png](chavo-ramon-acapulco.png) ][chavito-animado] |  
|:-----:|  
| Figure 1: _Don Ramón_ and _El Chavo del 8_ in _Acapulco_. |

[chavito-animado]: https://youtu.be/7OHV4NVj3gM

## The backstory

After walking for _2 days, 7 hours, 27 minutes, and about 36 seconds, Don Ramón_
and _el chavo_ finally made it to _Acapulco_. Needless to say that they are
excited and want to explore the city and buy food and souvenirs. Unfortunately,
things are not quite as straightforward as in _Mexico City_: prices are listed
in foreign currencies and for some reason stores do not list the unit price of a
product. _El chavo_ wants to buy his favorite food: _'torta de jamón'_ (ham
sandwich, that is). However, he is confused as to what is its price; whereas one
store has the item listed as **4 tortas = 20 dollars**, another one lists the
same item as **5 tortas = 15 euros**. Since he and _Don Ramón_ are already short
on cash, they do not want to spend more money than what they should. They could
try figuring out the conversions and unit prices but as you know, they are not
very good with math.

> _Y ahora, ¿quién podrá ayudarlos?_ [Who can help them now?].


## Your assignment

Once again you are to play the role of _el chapulín colorado_.

Write a program that solves this problem. The idea is to read some information
from the console and calculate the exchange rate between currencies as well as
the unit price of the inputted item. What makes this problem a little harder is
that your program needs to be able to handle any type of currency.

To give you a better idea of what you need to do I have included (below) two
sample runs of the program.

*   First run

    ~~~~~
    Enter the desired purchase: 4 tortas
    Enter the price: 12.24 swiss-francs
    Enter the exchange rate (in pesos), 1 swiss-franc = 5.5

    Currency exchange:
    swiss-franc-to-peso rate     5.50
    peso-to-swiss-franc rate     0.18
    Unit prices:
    in swiss-francs              3.06
    in pesos                    16.83
    ~~~~~

    The user inputs `4 tortas` (1st line), `12.24 swiss-francs` (2nd line), and
    `5.5` (3rd line). The rest of the output is produced by the program.

*   Second run

    ~~~~~
    Enter the desired purchase: 3 beach-balls
    Enter the price: 9 pesos
    Enter the exchange rate (in pesos), 1 peso = 1

    Currency exchange:
    peso-to-peso rate     1.00
    peso-to-peso rate     1.00
    Unit prices:
    in pesos              3.00
    in pesos              3.00
    ~~~~~

    The user inputs `3 beach-balls` (1st line), `9 pesos` (2nd line), and `1`
    (3rd line). The rest of the output is produced by the program.

> **Please note:** the first column adjusts to the length of the words.


## Formatting

Your output should be formatted in 2 columns, as shown above. Keep in mind that
the **input currencies may be arbitrarily long** and your formatting for the
**first column should adjust accordingly**. Also, the exchange rates should be
reported with **exactly 2 decimal spaces**.

For this assignment you may assume:

*   All input exchange rates are short enough so that a width of 8 for the
    second column is sufficient.

*   When prompted for the desired purchase, the user will enter an **integer
    followed by the plural of an item with no blank spaces** (e.g., _beach
    balls_ is entered as `beach-balls`).

*   When prompted for the price, the user will enter a **double followed by the
    plural of a currency with no blank spaces** (e.g., _swiss francs_ is entered
    as `swiss-francs`).

*   The _pluralization_ involves only adding _'s'_ to the end of the item or
    currency. Valid items include _'tortas'_ and _'ducks'_ as well as the
    currencies _'pesos'_ and _'euros'_. Examples of invalid inputs are _'feet'_
    and _'tamales'_ (contrary to what some of you might think, the singular form
    of _tamales_ is _tamal_ and not _tamale_).


## Submission

Place your code in a source file labeled `hw2.cpp` (all lowercase). If your file
is not named exactly like this, your homework might not be graded. Your code
should **contain useful comments** as well as **your name**, **the date**, and a
**brief description of what the program does**. Upload your file to the
[assignments section of the CCLE website][ccle-hw]. The file will be
automatically collected at the date and time listed in _"submission status"_
table at the bottom of the CCLE assignment description corresponding to this
project.

[ccle-hw]: https://ccle.ucla.edu/course/view.php?id=67961&section=4on=2


### Grading rubric

Points will be assigned based on the following categories:

| **Category** | **Description** | **Points** |  
|:-----|:----------------------------------------------------------|:---:|  
| **Correctness** | No errors and the calculations are accurate. | 4 |  
| **Input/Output** |  Handles input/output correctly. The output is formatted according to the guidelines. The column widths adjusts to the lengths of the currencies. | 4 |  
| **Strings** | Correctly manipulates all strings. | 4 |  
| **Solution** | The code is efficient but easy to follow. | 4 |  
| **Style** | Variable names, comments, indentation, etc. | 4 |  


---

[Return to main course website][PIC]

[PIC]: ../..
