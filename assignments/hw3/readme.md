# Coding assignment: a _gato_ game

| [ ![chavo-carrito.png](chavo-carrito.png) ][chavito-carro] |  
|:-----:|  
| Figure 1: _El Chavo_ riding a _mini-guajolotero_. |

[chavito-carro]: https://youtu.be/7jZCVHo_E2Q

## The backstory

After spending most of their money on _tortas de jamón_, _Don Ramón_ and _el
chavo_ are ready to head home. They were wise enough to save some money for the
bus tickets but since ---as you know--- they are very poor, they could not
afford to buy a ticket for the _atobus de lujo_ (deluxe bus) and had to settle
for a _guajolotero_ bus (which stands for a bus that carries around
_guajolotes_, the word used in _México_ to refer to turkey birds).

Whereas the deluxe bus travels along a toll road and completes the trip to
Mexico City in about 4 hours, the _guajolotero_ bus travels along a
not-so-well-maintained highway, and it could take up to 24 hours to complete the
trip. Of course this is a long time and they will get really, really, really
bored. If they only had a game or something that could help them pass the
time...

> _Y ahora, ¿quién podrá ayudarlos?_


## The assignment

You are to write a game called _gato_. That is Tic-Tac-Toe, for those of you
that have not heard the name in Spanish. First you need to draw the board using
classes from the graphics library discussed in class. Next, ask the user to
click on a empty spot on the board and display alternating X's and O's on the
locations of those mouse clicks. After 9 clicks, presumably when the board has
been filled up, the game ends. You must display a "Game over" message somewhere
on the viewing window and proceed to ask the user whether or not another game is
to be played. The program should end when the user enters a negative response to
this question.

For this assignment you may assume:

*   The user knows how to play the game; that is, once a location of the board
    is occupied, the user will not try to click on that location again unless a
    new game has started.
*   The game ends after 9 clicks regardless of whether or not there is an actual
    winner.
*   The player who draws the O's always gets to start the game (_i.e.,_ when the
    board is empty the first symbol displayed will be an O).

Notice that there could be many individual games of _gato_ every time the
program is run. You will not know in advance whether the user wants to play 1,
or 100 games; your program should adjust accordingly. Also, notice that at every
individual game, exactly 5 O's and 4 X's are to be drawn.

The purpose of this assignment is that you code your program efficiently. You
should not repeat the procedure for drawing an O or an X more than necessary. In
other words, you should loop the part of your code that performs the drawing
rather than simply repeating your code multiple times. 

Here is some more information about your assignment:

*   You **are not expected** to write code that plays the game of Tic-Tac-Toe
    against the user. Instead, the user will choose all locations for all of the
    O's and all of the X's.
*   You **are not responsible** for tracking poor mouse clicks by the user, such
    as not clicking within a box or a user clicking all 9 X's and O's in the
    same box (remember: you are under the assumption that the user knows how to
    play the game).
*   You **are not responsible** for deciding whether or not there is a winner.


## Extra credit

The following features of your program could result in extra credit on your
homework score. I strongly suggest you get the basic program to work before
attempting to implement any of these features. Neither I nor any of the TA's
will give you any help with the extra credit.

*   (+1 point) Instead of drawing an O, sketch a _guajolotero_ bus. To get this
    point, it should be absolutely clear that your sketch indeed represents a
    _guajolotero_ bus and not just a generic bus.

*   (+1 point) End the game at the first occurrence of a 3-in-a-row. In this
    case, a line through the winning 3 spots must be drawn and you must declare
    the winner: O or X.

*   (+2 points) Write an intelligent Tic-Tac-Toe playing system so that the
    computer plays against the user.


## Submission

Place your code in a source file labeled `hw3.cpp` (all lowercase). If your file
is not named exactly like this, your homework might not be graded. Your code
should **contain useful comments** as well as **your name**, **the date**, and a
**brief description of what the program does**. Upload your file to the
[assignments section of the CCLE website][ccle-hw]. The file will be
automatically collected at the date and time listed in _"submission status"_
table at the bottom of the CCLE assignment description corresponding to this
project.

[ccle-hw]: https://ccle.ucla.edu/course/view.php?id=67961&section=4on=2


## Grading rubric

| **Category** | **Description** | **Points** |  
|:---------------|:------------------------------|:----------:|  
| **Correctness** | No errors. The game is played correctly and the program ends when instructed. | 4 |  
| **Graphics** | Draws grid. Records mouse clicks. Outputs O and X correctly. | 4 |  
| **Input/Output** | The output (_e.g.,_ messages) clearly instructs the user what to do. The input is correctly interpreted. | 4 |  
| **Solution** | The code is efficient but easy to follow. There is no unnecessary repetition of code. | 4 |  
| **Style** | Variable names, comments, indentation, etc. | 4 |  
| **Total** | | 20 | 


---

[Return to main course website][PIC]

[PIC]: ../..
