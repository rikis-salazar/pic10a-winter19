# Horstmann Graphics on Mac OS X via Visual Studio Code

## Requirements

Make sure [Xquartz][xq] is installed in your Mac. If this is not the case,
install it and reboot your computer.

[xq]: https://www.xquartz.org/

## The steps

1.  [Download and install Visual Studio Code for Mac.][VSC]

    > **Note:**
    > 
    > You might need to move the program from your `Downloads` folder to your
    > `Applications` folder.

1.  Follow the instructions in the Getting started section [here][ext] to add
    the Microsoft C/C++ extension.
    
    > **Note:**
    >
    > You can safely ignore the rest of the set-up, I already created and
    > modified the `.json` files they mention therein.

1.  [Download the Visual Studio Code Setup for Graphics from CCLE.][ccle]

1.  Extract the contents of the `.zip` file in step 4, above. Place the
    extracted files, that is the whole `VSCodeGraphics` directory, in a location
    that is easily accessible (_e.g.,_ your Desktop).

1.  Open the `VSCodeGraphics` folder in Visual Studio Code.

    > **Note:**
    >
    > You need to open the whole folder as opposed to individual files. Use the
    > Command-O shortcut, then locate the folder and press the open button.

1.  Test the project by building and running the code. Click the _Debug_ menu
    entry and select _Start without debugging_.
    
If things went well, a window with a couple of circles and a line will pop up.

[VSC]: https://code.visualstudio.com/download
[ext]: https://code.visualstudio.com/docs/languages/cpp
[ccle]: https://ccle.ucla.edu/mod/resource/view.php?id=2274319

### Writing your code

The current setup of the `VSCodeGraphics` is a bit more advanced than what is
needed for the _tic-tac-toe_ game, as well as the _trip planner_ program. You
need to make sure the library file `lib_file.h` is not actually included in your
project. Fortunately this is a piece of cake: simply remove or comment out the
second line of `main.cpp`.

After this change, proceed to replace the contents of the `ccc_win_main()`
function with your code. Your project then should look like this:

~~~~~ {.cpp}  
#include "ccc_win.h"
// #include "lib_file.h"

using namespace std;   // <-- Not actually needed.

int ccc_win_main() {
    // YOUR CODE HERE!

    return 0;
}
~~~~~ 
    
And you should be good to go to write graphics programs in your Mac.

---

[Return to main course website][PIC]

[PIC]: ../..
