# Handouts and how-to guides/demos 

This section is for miscellaneous material that does not quite fit in other
sections.

<!--

## Regrades

*   [Requesting a regrade][link1].

-->

## Horstmann graphics

**Microsoft Windows**

*   [Visual Studio 2017 (written instructions)][link2]
*   [Visual Studio 2017 (video)][link3]: based on the written instructions in
    the link above.

    > **Note:**
    >
    > The setup described in the previous two links requiere you to _move_ a set
    > of downloaded files into a special folder created by Visual Studio. This
    > means that every time you create a new project, [a copy of] those 5 files
    > will have to be redownloaded and _moved_ (or _copied_) over to a different
    > location.

*   [Visual Studio 2017 (handout + short video)][link4]: a slightly more
    difficult setup that does not requiere extra copies of the `ccc` files.


**Mac OS X**

*   [Visual Studio Code (written instructions)][link5]
*   [Visual Studio Code (video)][link6]: based on the written instructions in
    the link above.

    > **Note:**
    >
    > The links above refere to Visual Studio Code, which is not the same as
    > Visual Studio for Mac.

*   [Xcode (video)][link7]: based on instructions sent to some of you by _Josh
    Keneda_.

    > **Note:**
    >
    > The video turned out to be a bit low resolution. The info listed here
    > corresponds to the _strings_ you need to enter when configuring your
    > project:
    >
    > -   In _"Header Search Paths"_, type `/opt/X11/include`.  
    > -   In _"Library Search Paths"_, type `/opt/X11/lib`.  
    > -   In _"Other Linker Flags"_, type `-lX11`. That is _dash_, _lower-case
    >     ell_, followed by _eleven_.

[link1]: regrades/
[link2]: https://www.pic.ucla.edu/how-to-create-a-c-graphics-application/
[link3]: https://www.youtube.com/watch?v=e9HA1uiRCeA
[link4]: vs2017-no-copies/
[link5]: vs-code-for-mac/
[link6]: https://youtu.be/NnxFKHezwjw
[link7]: https://www.youtube.com/watch?v=JWupRku64QQ

---

[Return to main course website][PIC]

[PIC]: ..

