/* *****************************************************
   Ricardo Salazar
   February 2, 2015

   Example:
   The 'complex' problem of prime factorization.  Here we tackle the algorithm
   using a stub!  We test for primes... assuming all numbers are prime!

   Keywords: 
   functions, return values, predicates, stubs
   ***************************************************** */
#include <iostream>
using namespace std;

// Stub function. It returns true. Its only purpose is to make sure the _whole
// program_ can be put together, and the other components are tested.
bool isPrime( int n ){
    return true;
}

int main(){
   // int N = 360;
   int N = 132;

   // See example on Lesson 11 for an explanation of this loop
    int i = 2;
    while ( i <= N ){
        while( isPrime(i)  &&  N % i == 0 ){
            cout << i << " ";
            N /= i;
        }
      
        // Displays a new line every time a prime is found.  In this case it
        // displays one line per number because our stub function always returns
        // `true`.
        if ( isPrime(i) )
            cout << endl;
        i++;
    }
   
    return 0;
}
