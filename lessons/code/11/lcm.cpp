/* *****************************************************
   Ricardo Salazar
   February 2, 2015

   Example:
   The least common multiple of two numbers.

   Keywords: 
   reusing functions, translation of pseudo-code.
   ***************************************************** */
#include <iostream>
using namespace std;

/* ****************************************************
   isPrime  test to determine whether a positive number
            is prime.
   Parameters:
      n    a positive prime.

   Returns: true if n is prime, false otherwise.
   **************************************************** */
bool isPrime( int n ){
    if ( n <= 1 )
        return false;

    if ( n == 2 )
        return true;

    if ( n % 2 == 0 )
        return false;

    int k = 3;
    while ( k * k <= n ) {
        if (n % k == 0)
            return false;
        k = k + 2;
    }

    return true;
}

/* ****************************************************
   leastCommonMultiple:
           computes the least common multiple of
           two integers

   Parameters:
      N    positive prime.
      M    positive prime.

   Returns: the least common multiple of N and M.

   If you followed the slides and actually solved the
   exercise at the end of lesson 11, you should have
   arrived to some similar implementation.
   **************************************************** */
int leastCommonMultiple( int N, int M ){
    int lcm = 1;
    for ( int i = 2 ; i <= N  ||  i <= M ; i++ ){
        while( isPrime(i)  &&  ( N % i == 0  ||  M % i == 0 ) ){
            lcm *= i;
            if ( N % i == 0 )
                N /= i;
            if ( M % i == 0 )
                M /= i;
        }
    }
    return lcm;
}

/* ****************************************************
   greatestCommonDivisor: 
           computes the greatest common divisor of 
	   two integers

   Parameters:
      N    positive prime.
      M    positive prime.

   Returns: the greatest common divisor of N and M.

int greatestCommonDivisor( int N, int M ){
    int gcd = 1;
    for ( int i = 2 ; i <= N && i <= M ; i++ ){
        while( isPrime(i)  &&   N % i == 0  &&  M % i == 0 ){
            gcd *= i;
            N /= i;
            M /= i;
        }
    }
    return gcd;
}

//   cout << "The g.c.d. is " << greatestCommonDivisor(a,b) << endl;
//   cout << "The product is " 
//        << leastCommonMultiple(a,b) * greatestCommonDivisor(a,b) << endl;
   **************************************************** */

int main(){
   int a, b;
   cout << "Enter 2 positive integers: ";
   cin >> a >> b;

   cout << "The l.c.m. is " << leastCommonMultiple(a,b) << endl;

   return 0;
}
