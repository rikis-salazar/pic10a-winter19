// Division, casting and rounding:
//
// Some decimal values are difficult to handle properly.
//
// UCLA Pic 10A Prof. Salazar

#include <iostream>     // std::cout, std::cin

using namespace std;

int main() {
    const double PI = 3.14;
    const double TROUBLE = 4.35;

    double x = 5 / 3;
    double y = 5.0 / 3;
    cout << "double x = 5 / 3;\t// sets x = "<< x << '\n';
    cout << "double y = 5 / 3.0;\t// sets y = "<< y << '\n';
    cout << '\n';

    // Rounding errors: this produces no warnings because of casting
    int r = static_cast<int>( TROUBLE * 100 );       // Gives incorrect value
    int s = static_cast<int>( TROUBLE * 100 + 0.5 ); // Gives correct value

    // Rounding errors: the compiler might throw a warning here.
    // int r = TROUBLE * 100 ;
    // int s = TROUBLE * 100 + 0.5 ;

    cout << "int r = 4.35 * 100;\t// sets r = "<< r << ".\n";
    cout << "This is not correct, the actual value is " << s << ".\n";
    cout << '\n'; 

    // The roundoff error shown here is actually not very common, however you
    // should be aware it might occur.
    int p = static_cast<int>( PI * 100 );          // Gives correct value
    // int q = static_cast<int>( PI * 100 + 0.5 ); // Gives also correct value
    cout << "int p = 3.14 * 100;\t// sets p = "<< p << ".\n";
    cout << "This is correct. Compare to the example above.\n";

    return 0;
}

/**
   Sample output:
  
    double x = 5 / 3;	// sets x = 1
    double y = 5 / 3.0;	// sets y = 1.66667

    int r = 4.35 * 100;	// sets r = 434.
    This is not correct, the actual value is 435.

    int p = 3.14 * 100;	// sets p = 314.
    This is correct. Compare to the example above.
**/
