// Constants, accumulators & reusable variables:
//
// Computes the amount of soda in a fridge that contains 2-liter bottles, and
// 12-oz cans. It uses only one variable to read data from the user, and one
// more variable to keep track of the amount of soda.
//
// UCLA Pic 10A Prof. Salazar

#include <iostream>     // std::cout, std::cin

using namespace std;

int main() {
    // Constants
    const double BOTTLE_VOLUME = 2.0;
    const double CAN_VOLUME = 0.355;

    // Temporary storage for number of bottles, as well as the number of cans.
    int userInput;

    // Accumulator variable used to keep track of the total amount of liquid.
    double soda = 0.0;

    // Read bottles and update accumulator
    cout << "How many bottles? ";
    cin >> userInput;
    soda = soda + userInput * BOTTLE_VOLUME ;

    // Read cans and update accumulator
    cout << "How many cans? ";
    cin >> userInput;
    soda = soda + userInput * CAN_VOLUME ;

    // Show total
    cout << "The total volume is " << soda << " liter.\n";

   return 0;
}

/**
   Sample output:
  
    How many bottles? 32
    How many cans? 23
    The total volume is 72.165 liter.
**/
