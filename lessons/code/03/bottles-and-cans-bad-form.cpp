// Conversion program:
//
// Computes the amount of soda in a fridge that contains 2-liter bottles, and
// 12-oz cans.
//
// UCLA Pic 10A Prof. Salazar

#include <iostream>     // std::cout, std::cin

using namespace std;

int main() {
    // Read bottles
    int bottles;
    cout << "How many bottles? ";
    cin >> bottles;

    // Read cans
    int cans;
    cout << "How many cans? ";
    cin >> cans;

    // Compute and display total amount of soda
    double total = bottles * 2.0 + cans * 0.355;
    cout << "The total volume is " << total << " liter.\n";

   return 0;
}

/**
   Sample output:
  
    How many bottles? 32
    How many cans? 23
    The total volume is 72.165 liter.
**/
