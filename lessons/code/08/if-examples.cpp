#include <iostream>
using namespace std;

int main(){
    int a = 2;

    /**
    // With braces
    if ( a > 3 ){
        cout << "Chavo ";
        cout << "del ";
    }
    cout << 8;
    **/


    /**
    // Without braces
    if ( a > 3 )
        cout << "Chavo ";
        cout << "del ";
   
    cout << 8;
    **/

    /**
    // With semi-colon
    if ( a > 3 ) ;
        cout << "Chavo ";
        cout << "del ";

    cout << 8;
    **/

   return 0;
}
