// Vectors
//     - Construct
//     - Populate
//     - Resize
//     - Display
//     - Copy
//     - In functions as parameters
//     - In functions as return types
//     - Sort

#include <iostream>
#include <string>
#include <vector>

// Extra tools
#include <algorithm>

using namespace std;

class Cosa{
  public:

    Cosa();
    Cosa(double , string );

    void print_Cosa() const;

    double get_value() const ;
    string get_name() const ;

    void set_name( string );
    void set_value( double );

    bool operator<( Cosa ) const ;
    bool operator>( Cosa ) const ;

  private:
    double value;
    string name;
};

void print_array( int v[], int size );
void print_vector( vector<int> );
void print_vector( const vector<double>& );

// vector<Cosa> w = get_min_and_max(v_Cosa);
vector<int> get_min_and_max( vector<int> v ){
    // create result vector
    vector<int> result(2);
    // sort v
    sort(v.begin(),v.end());

    // result[0] = v[0];
    // result[1] = v[ v.size() - 1 ];
    result[0] = v.front();
    result[1] = v.back();
    return result;
}

int main(){

    Cosa c1;
    const Cosa c2(3.14,"jhdgkhjs");

    vector<int> v_int;
    vector<double> v_double(5);
    vector<Cosa> v_Cosa(2);

    v_Cosa.push_back(c1);
    v_Cosa.push_back(c2);

    v_int.push_back(4);
    v_int.push_back(4);
    v_int.push_back(4);
    v_int.push_back(4);


    v_double.push_back(4.321);
    v_double.push_back(4.321);
    v_double.push_back(4.321);
    v_double.push_back(4.321);


    print_vector(v_int);
    print_vector(v_double);

    for ( int i = 0; i < v_Cosa.size() ; ++i ){
        v_Cosa[i].print_Cosa();
    }
    cout << endl;

    c1.set_value(-3.21);

    c1.print_Cosa();
    c2.print_Cosa();

    // vector<int> w = get_min_and_max(v_int);
    print_vector( get_min_and_max(v_int) );

    int freeways[] = {101, 91, 710, 10, 90};
    // cout << freeways[0];
    cout << freeways << '\n';

    print_array(freeways,5);

    cout << freeways[0] << '\n';

/**
    for ( int i = -1e100 ; i < 25 ; i++)
        cout << ' ' << freeways[i];
    cout << '\n';
*/



/**
    if  ( c2 > c1 )
        cout << "c2 is better!";
    else
        cout << "c1 is better!";

    cout << "Hello, world!\n";
*/

    return 0;

}


// Default cosas
Cosa::Cosa(){
    value = 2018;
    name = "No name.";
}

Cosa::Cosa( double v, string s){
    value = v;
    name = s;
}

string Cosa::get_name( ) const {
    return name;
}


double Cosa::get_value( ) const {
    return value;
}

bool Cosa::operator<( Cosa rhs) const {
    if ( value < rhs.value )
        return true;
    return false;
}
bool Cosa::operator>( Cosa rhs ) const {
    if ( value > rhs.value )
        return true;
    return false;
}


void Cosa::set_value( double v){
    value = v;
}
void Cosa::set_name( string s){
    name = s;
}

void Cosa::print_Cosa( ) const {
    cout << "Value: " << value << endl
         << "Name: "  << name << endl;
}

void print_vector( vector<int> v ){
    for ( int i = 0; i < v.size() ; ++i )
       cout << " " << v[i];
    cout << endl;
    v[0] = 2018;
}

void print_vector( const vector<double>& v ){
    for ( int i = 0; i < v.size() ; ++i )
       cout << " " << v[i];
    cout << endl;
}


void print_array( int v[], int size ){
    v[0] = 2018;
    for ( int i = 0; i < size ; ++i )
       cout << " " << v[i];
    cout << endl;
}


