#include <iostream>
using namespace std;

int main(){
    double someNumbers[]={2, -3, 5, -7, 11, -13, 17, -19};

    cout << someNumbers[5] << endl;

    cout << someNumbers << endl;

    cout << ( someNumbers + 1 ) << endl;

/*
    int myArray[]={10,9,8};

    for (int i=0 ; i<=13 ; i++)
        cout << myArray[i] << " ";
    cout << endl;

    // Depending on the sensitivity of your compiler, 
    // the following two lines might or might not give 
    // you an error.
    int s1=3;
    double incorrectDeclaration[s1];

    // These two lines should be OK.
    const int s2=3;
    double correctDeclaration[s2];

    // Now let the user decide!?
    int s3;
    cin >> s3;
    double doesItWork[s3];
    */

    return 0;
}
