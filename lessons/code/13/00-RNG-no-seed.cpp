/* *********************************************
   Ricardo Salazar
   February 7, 2015

   Example:
      List of random numbers that can be 
      reproduced over and over again.

   Keywords: 
      random numbers, no seed, RAND_MAX
   ********************************************* */
#include <iostream>
#include <cstdlib>
using namespace std;

int main(){

    for ( int n = 1 ; n <= 5 ; n++ ){
        int r = rand();
        cout << r << endl;
    }
    cout << "Max random is " << RAND_MAX << endl;

    return 0;
}
