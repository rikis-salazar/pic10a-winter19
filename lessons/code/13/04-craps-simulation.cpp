/* *********************************************
   Ricardo Salazar
   February 9, 2015

   Example:
      A whole buch of games of craps (simulation).

   Keywords: 
      craps, random numbers, simulations, 
      averages.
   ********************************************* */

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

// total number of games that will be played
const int MAX_RUNS = 1e6;

int rollDice(){
    int die1 = 1 + rand()%6;
    int die2 = 1 + rand()%6;
    return die1 + die2;
}

int main(){
    // seeding the randome number generator
    srand( static_cast<int>( time(0) ));

    // initialization: make sure we do not
    // run out of money.
    int money = MAX_RUNS; 
    int bet = 1;
    double totalTurns=0.0;
    double loss;

    // play MAX_RUNS games
    for ( int runs = 1 ; runs <= MAX_RUNS ; runs++){
        int turns=1; 
        // Why simulate two dice if we just care about the sum
        // int sum = 2 + rand()%11
        int sum = rollDice();

        int points;
        switch (sum){
            case  2:
            case  3:
            case 12:
                money -= bet;
                break;

            case  7:
            case 11:
                money += bet;
                break;

            case  4:
            case  5:
            case  6:
            case  8:
            case  9:
            case 10:
                points = sum;
                do{
                    turns++;
                    sum = rollDice();
                }while( sum!=7 && sum!=points );

                if ( sum==7 )
                    money -= bet;
                else
                    money += bet;

                break;

         // no need for the next line but good practice to include it
            default:
                break;
        }
        totalTurns += turns;

    }

    cout << "Average turns = " << totalTurns/MAX_RUNS << endl;
    cout << "Balance = " << money << endl;

    // I know it is a loss because I have taught Math 170A
    loss = static_cast<double>(money-MAX_RUNS)/MAX_RUNS;
    cout << "Your average loss is " << loss << endl;
}
