/* *********************************************
   Ricardo Salazar
   February 7, 2015

   Example:
      Rolling a pair of dice (simulation).

   Keywords: 
      random numbers, simulation. 
   ********************************************* */
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main(){
    srand( static_cast<int>( time(0) ) );
    for ( int n=1 ; n <= 20 ; n++ ){
        int die1 = 1 + rand() % 6;
        int die2 = 1 + rand() % 6;
        cout << die1 << " " << die2 << endl;
    }
    return 0;
}
