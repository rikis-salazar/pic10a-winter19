/* *********************************************
   Ricardo Salazar
   February 7, 2015

   Example:
      List of random numbers.

   Keywords: 
      random numbers, seeding.
   ********************************************* */

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main(){
    srand( static_cast<int>( time(0) ) );
    for ( int n = 1 ; n <= 5 ; n++ ){
        int r = rand();
        cout << r << endl;
    }
    return 0;
}
