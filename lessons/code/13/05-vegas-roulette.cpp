/* *********************************************
   Ricardo Salazar
   February 9, 2015

   Example:
      Playing roulette 1 million times (simulation).
      We always bet one dolar on red.

   Keywords:
      Las Vegas roulette, random numbers,
      simulation, enumerated types.

   The Las Vegas wheel:
      A roulette in Las Vegas consists of 38 spots
      18 red, 18 black plus two green ones labeled
      0 and 00. 
   ********************************************* */

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;


// Enumerated type: color_t
enum color_t { RED, BLACK, GREEN };


// Global constants:
// Uncomment either of the two lines (one at a time)
// to play 5 or 1 million times.
//
// const int RUNS = 1e6;
const int RUNS = 5;


// Function declarations
int spinRoulette();
int randomNumberBetween(int a, int b);
color_t colorOf(int r);


/* *********************************************
   randomNumberBetween
      Generates random numbers between two given
      integers.

   parameters:
      two integers a and b.

   returns:
      A random number between two given integers.
   ********************************************* */
int randomNumberBetween(int a, int b){
    if ( a <  b )
        return a + rand() % ( b - a + 1 );
    else if ( b < a )
        return b + rand() % ( a - b + 1 );
    else
        return a;
}


/* *********************************************
   colorOf
      Obtains the color of the roulette spot.

   parameters:
      an integer between -1 and 36.

   returns:
      GREEN if r is -1 (spot 00) or 0 (spot 0), 
      BLACK if r is 2, 4, ... , 36
      RED   if r is 1, 3, ... , 35 
   ********************************************* */
color_t colorOf(int r){
    if ( r <= 0 ){
        return GREEN;
    }
    else if ( r % 2 == 0 ){
        return BLACK;
    }
    else{
        return RED;
    }
}


/* *********************************************
   spinRoulette
      A simple function that implements some set
      of simple rules to play a game of roulette

   Parameters:
      no parameters.

   Returns: 
       1  if the roulette lands on RED.
      -1  if it lands on BLACK or GREEN.

   Side effects:
      displays a message indicating the  number 
      and color, as well as whether the current
      game is a win or a loss.
   ********************************************* */
int spinRoulette(){
    int r = randomNumberBetween(-1,36); 
    color_t c = colorOf( r );

    if ( r == -1)
        cout << "00";
    else
        cout << r;

    switch ( c ){
        case GREEN: // Lose bet
            cout << " Green. Lose bet!" << endl; 
            return -1;
            break;

        case BLACK: // Lose bet
            cout << " Black. Lose bet!" << endl;
            return -1;
            break;

        case RED:   // Win bet
            cout << " Red. Win bet!" << endl;
            return 1;
            break;
    }
}


/* *********************************************
   main
   ********************************************* */
int main(){
    // comment the next line to reproduce the same
    // sequence of 'random' numbers
    srand( static_cast<int>( time(0) ));

    // Assume we bet one dollar on red every time,
    // play as many games as dollars we have initially.
    int money = RUNS;

    for ( int n=1; n <= RUNS ; n++ )
        money += spinRoulette();

    cout << "Money left: " << money << endl;
    return 0;
}
