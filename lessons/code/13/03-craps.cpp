/* *********************************************
   Ricardo Salazar
   February 9, 2015

   Example:
      One single game of craps (simulation).

   Keywords: 
      craps, random numbers
   ********************************************* */

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

// Practice passing values by reference.
void rollDice(int& d1, int& d2){
    d1 = 1 + rand()%6;
    d2 = 1 + rand()%6;
    return; 
}

int main(){
    // seeding the random number generator
    srand( static_cast<int>( time(0) ));

    // Initialization
    int bet = 1;
    int turns=1;
    int die1,die2;

    // Come out phase
    cout << "Rolls\n";
    cout << "Come out phase:\n";
    rollDice(die1,die2);
    cout << die1 << " " << die2 << endl;
    int sum = die1 + die2;

    // Points phase
    // Can also be implemented with if/else-if/else
    int points;
    switch (sum){
        case  2:
        case  3:
        case 12:
            cout << "You lose!\nTurns = " << turns << endl;
            break;

        case  7:
        case 11: 
            cout << "You win!\nTurns = " << turns << endl;
            break;

        case  4:
        case  5:
        case  6:
        case  8:
        case  9:
        case 10:
            points = sum;
            do{
                cout << "Point phase ("<< points <<")\n";
                turns++;
                rollDice(die1,die2);
                cout << die1 << " " << die2 << endl;
                sum = die1 + die2;
            }while( sum!=7 && sum!=points ); // De Morgan's Laws

            if ( sum==7 ){ 
                cout << "You lose!\nTurns = " << turns << endl;
            }
            else{
                cout << "You win!\nTurns = " << turns << endl;
            }
            break;

        default:
            break;
    }
}
