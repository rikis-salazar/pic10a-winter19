#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main(){
    vector<int> v(4);
    v[0]=25;
    v[1]=2;
    v[2]=13;
    v[3]=9;

    sort( v.begin() , v.end() );
    for ( int i=0 ; i < v.size() ; i++ )
        cout << v[i] << " ";

    cout << endl;
    return 0;
}
