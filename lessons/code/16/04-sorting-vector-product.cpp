// This program needs the `product.h` library from Lesson 15.
#include <iostream>
#include <vector>
#include <algorithm>
#include "product.h"

using namespace std;

int main(){
    vector<Product> v(4);
    v[0]=Product("Chavo del 8 (DVD)", 0, 10);
    v[1]=Product("Quiko's balon (soccer ball)", 10.05, 8);
    v[2]=Product("Don Ramon (deck of cards)", 5.99, 8);
    v[3]=Product("Torta de huevo (egg sandwich)",12.55,2);

    sort( v.begin() , v.end() );
    cout << "From worst to best:" << endl;
    for ( int i=0 ; i < v.size() ; i++ ){
        cout << endl;
        v[i].print();
    }
    return 0;

}
