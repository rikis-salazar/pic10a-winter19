#include <vector>
#include <iostream>

using namespace std;

// Function declarations
void print_vector_of_ints( vector<int> the_vector );
vector<int> create_vector_with_ones( int size );



// Function definitions
vector<int> create_vector_with_ones( int size ){
    // Create local vector with appropriate size
    vector<int> local_vector(size);
    
    // Replace the entries with desired values
    for ( int i = 0 ; i < local_vector.size() ; ++i )
        local_vector[i] = 1;

    // Return the local vector
    return local_vector;
}

void print_vector_of_ints( vector<int> the_vector ){
    // The loop
    //     for ( int x : the_vector )
    //         cout << " " << x;
    //
    //  is known as a range based loop. In this case it is equivalent to
    for ( int i = 0 ; i < the_vector.size() ; ++i )
        cout << " " << the_vector[i];
    cout << '\n';

    the_vector.resize(0);
    // The local copy is the one that is resized, the original vector remains
    // unchanged.
}

int main(){

    vector<int> v;

    v.push_back(10);
    v.push_back(2019);
    v.push_back(3);

    // Prints all elements in vector
    for ( int i = 0 ; i < v.size() ; ++i )
        cout << ' ' << v[i]; 
    cout << '\n';

    // Prints all elements in vector (again)
    print_vector_of_ints(v);    // <-- Vectors can be passed as parameters


    // Prints elements in other vector
    print_vector_of_ints( create_vector_with_ones(5) );
    //                    \------_____._____------/
    // Functions can also return vectors

    return 0;
}
