// `string` member functions:
//
// The string class: using member functions `insert`, `erase`, and `find`.
//
// UCLA Pic 10A Prof. Salazar

#include <iostream>     // std::cout
#include <string>       // std::string, string::erase(...), string::find(...),
                        // string::insert(...)
using namespace std;

int main() {
    string s1 = "Don Ramon";
    string s2 = "El Chavo";

    s1.insert( 4, s2.substr(3,3) + " " );
    // What happens if the arguments are reversed?
    /// s1.insert( s2.substr(3,3) + " " , 4 );
    cout << s1 << '\n';

    s1.insert( 4, "Chi " );
    cout << s1 << '\n';

    s1.erase( 13, 2 );
    cout << s1 << '\n';
    s2 = s2.substr( 0, 3 ) + s1.erase( s1.find("Don"), 4 );
    cout << s2 << '\n';

    return 0;
}

/**
   Sample output:

    Don Cha Ramon
    Don Chi Cha Ramon
    Don Chi Cha Ron
    El Chi Cha Ron
**/
