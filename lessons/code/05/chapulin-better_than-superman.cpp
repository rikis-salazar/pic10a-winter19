// `string` class (yet one more example)
//
// The string class: using member functions `insert`, `erase`, and `find`.
//
// This program takes a complete line of text, then finds the first reference to
// ‘Superman’ and replaces it with the best super hero: ‘Chapulin Colorado’. 
//
// UCLA Pic 10A Prof. Salazar

#include <iostream>     // std::cout
#include <string>       // std::string, string::erase(...), string::find(...),
                        // string::insert(...)
using namespace std;

int main() {
    string textline;
    string superman = "Superman";

    cout << "Enter your text: ";
    getline( cin, textline );

    int ini = textline.find( superman, 0 );
    textline.erase( ini, superman.length() );
    textline.insert( ini, "Chapulin Colorado" );

    cout << "I beg to differ!\n";
    cout << textline << '\n';

    return 0;
}

/**
   Sample output:

    Enter your text: The best superhero is Superman.
    I beg to differ!
    The best superhero is Chapulin Colorado.
**/
