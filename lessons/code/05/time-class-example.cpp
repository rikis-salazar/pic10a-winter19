// `Time` class example
//
// Using some member functions of the `Time` class: this program computes the
// number of seconds between the current time and the time the next assignment
// is due.
//
// > **Note:**
// >
// > This program needs the files `ccc_time.h` and `ccc_time.cpp` provided by
// > author of our textbook. You may also need to perform additional steps in
// > your Visual Studio or Xcode setup in order to compile this example.
//
// UCLA Pic 10A Prof. Salazar

#include <iostream>     // std::cout
#include "ccc_time.h"   // Time(...), Time::seconds_from(...)
//       ^----------^   // The quotation marks indicate that this is a user
                        // defined library. The compiler might not know where to
                        // look for this file. This is usually dealt with by
                        // making sure the all project files are located within
                        // the same directory.
using namespace std;

int main() {
    Time currentTime;
    Time hw1Due( 17, 59, 59 );

    int remainingSeconds = hw1Due.seconds_from(currentTime);

    cout << "There are " << remainingSeconds
         << " before the assignment is due." << '\n';

    return 0;
}
/**
   Sample output:

    There are -3787 before the assignment is due.

   Note: this program was run on: Tue Jan 22 19:03:06 UTC 2019
**/
