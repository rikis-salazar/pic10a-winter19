/* *****************************************************
   Ricardo Salazar
   February 2, 2015

   Example:
   Displaying a table.

   Keywords: 
   Nested loops. 
   ***************************************************** */
#include <iostream>
using namespace std;

int main(){

   for(int denom=2 ; denom<6 ; denom++){
      for (int num=1 ; num <=7 ; num++){
         cout << num << "/" << denom << "\t";
      }
      cout << endl;
   }

   return 0;
}

