/* *****************************************************
   Ricardo Salazar
   February 2, 2015

   Example:
   A basic calculator.

   Keywords: 
   Switch statement, do-while
   ***************************************************** */
#include <iostream>
using namespace std;

int main(){
   int choice;
   double a, b;
   char response;
   bool conti;
   string dummy;

   do{ 
      cout << "Enter two numbers: "; 
      cin >> a >> b; 
      
      cout << "Operations:\n1) to add\n2) to multiply\n3) to divide\n\n"; 
      cout << "Choice: "; 
      cin >> choice; 
      
      cout << "The result is: "; 
      
      switch (choice){ 
         case 1: 
	          cout << a + b ; 
		  break;
	 case 2:
		  cout << a * b ; 
		  break;
	 case 3:
		  cout << a / b ; 
		  break;
	 default:
		  cout << "Error!\n"; 
		  break;
      }
      cout << "\nContinue (y/n)? ";
      
      // read first character of response
      cin >> response;

      // store the rest (upt to \n) into a dummy container
      // this clears the buffer.
      getline(cin,dummy);

      switch (response){
         case 'y':
         case 'Y':
	           conti = true;
		   break;
         default:
	           conti = false;
		   cout << "Bye!\n";
      }

   }while( conti );

   return 0;
}
