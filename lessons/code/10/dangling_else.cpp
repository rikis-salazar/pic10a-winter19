/* *****************************************************
   Ricardo Salazar
   February 2, 2015

   Example:
   The danglig else problem.

   Keywords: 
   Logic error, indentation.
   ***************************************************** */
#include <iostream>
#include <string>
using namespace std;

int main(){
   string firstName="Ricardo";
   string lastName="Sanchez";
   
   if ( firstName=="Ricardo" )
      if ( lastName=="Salazar" )
         cout << "You are Ricardo Salazar" << endl;
   
   else
      cout << firstName + " " + lastName 
           << ", your first name is not Ricardo" << endl;

   return 0;
}

