/* ********************************
   Ricardo Salazar  
   January 29, 2015
   
   Display Hello! 10 times... or more!
   Uncomment either of the blocks, 
   then compile it and run it.

   To stop it: 
   press <Ctrl>-<c> 
   ******************************** */
#include <iostream>
using namespace std;

int main(){
/*
// 1st Try:
   int i =1;
   while ( i <=10 )
      //cout << i << ": Hello!\n"; // displays the index
      cout << "Hello!\n";
      i++;
*/

/*
// 2nd Try:
*/
   double r = 4.35 * 100 - 434; // r = 1 ???
   //int j=1;
   //while ( r != 10 && j <= 10){
   while ( r != 10 ){
      //cout << static_cast<int>(r) << ": Hello!\n";
      //cout << j << ": Hello!\n";
      //j++;
      cout << "Hello\n";
      r++;
   }

   return 0;
}
