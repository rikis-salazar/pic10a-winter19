#include <fstream>
#include <iostream>
#include <iomanip>

using namespace std;


// The examples from the slides are "sent" to an output stream. This could be
// the console, of even a text file.
void print_examples_to( ostream& out ){
    // A simple ruler for reference purposes
    const string RULER = "\n---------1---------2\n";

    out << "out << setw(6) << 123;";
    out << RULER;
    out << setw(6) << 123;
    out << RULER;

    out << '\n';
    out << "out << setw(6) << setfill('*') << 123;";
    out << RULER;
    out << setw(6) << setfill('*') << 123;
    out << RULER;

    out << setfill(' ');  // Undoes the `cout << setfill('*')` above
    out << '\n';
    out << "out << setw(6) << left << 123;";
    out << RULER;
    out << setw(6) << left << 123;
    out << RULER;

    out << '\n';
    out << "out << setw(6) << right << 123;";
    out << RULER;
    out << setw(6) << right << 123;
    out << RULER;

    out << '\n';
    out << "cout << hex << 64206;    // 64206 = 0xface";
    out << RULER;
    out << hex << 64206;
    out << RULER;

    out << '\n';
    out << "cout << dec << 0xc0ffee; // 0xc0ffee = 12648430";
    out << RULER;
    out << dec << 0xc0ffee;
    out << RULER;

    out << '\n';
    out << "cout << 123.45;";
    out << RULER;
    out << 123.45;
    out << RULER;

    out << '\n';
    out << "cout << fixed << 123.45;";
    out << RULER;
    out << fixed << 123.45;
    out << RULER;

    out << defaultfloat;   // Undoes the `cout << fixed` above
    out << '\n';
    out << "cout << setprecision(2) << 987.6;          // General format";
    out << RULER;
    out << setprecision(2) << 987.6;
    out << RULER;

    out << '\n';
    out << "cout << setprecision(2) << fixed << 987.6; // Fixed format";
    out << RULER;
    out << setprecision(2) << fixed << 987.6;
    out << RULER;
}



// Manipulator examples.
int main(){

    ofstream fout;    // fout = file out (c.f. cout = console out)
    fout.open("output_file.txt");

    print_examples_to( fout );
    // print_examples_to( cout ); // Uncomment to see examples while running.

    fout.close();


    return 0;
}
