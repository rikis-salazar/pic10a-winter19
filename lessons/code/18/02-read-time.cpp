#include <string>
#include <iostream>
#include <sstream>

using namespace std;

 
// Converts an integer value to a string, e.g. 3 -> "3".   
string int_to_string( int n ) {  
    ostringstream oss;        // oss = output string stream
    oss << n;
    return oss.str();
}


// Reads time from standard input in formats
//     hh
//     hh am 
//     hh pm
//     hh:mm
//     hh:mm am
//     hh:mm pm
// Note that user can enter times that make little sense.
//    E.g.: 23:12 pm
void read_time( int& hours, int& minutes ) {  
    string line;
    getline(cin, line);

    istringstream iss( line.c_str() );  // iss = input string stream
    iss >> hours;

    minutes = 0;

    char ch;
    iss.get(ch);
    if (ch == ':')
        iss >> minutes;
    else
        iss.unget();

    string suffix;
    iss >> suffix;
    if ( suffix == "pm" )
        hours += 12;
}


// Computes a string representing a time.
string time_to_string( int hours, int minutes, bool military ) {
    string suffix;
    if ( !military ) {
        if ( hours < 12 )
            suffix = "am";
        else {
            suffix = "pm";
            hours = hours - 12;
        }

        if ( hours == 0 )
            hours = 12;
    }

    string result = int_to_string(hours) + ":";
    if ( minutes < 10 )
        result = result + "0";

    result = result + int_to_string(minutes);
    if ( !military )
        result = result + " " + suffix;

    return result;
}

int main() {
    cout << "Please enter the time: ";

    int hours;
    int minutes;
    read_time(hours, minutes);

    cout << "Military time: "
         << time_to_string(hours, minutes, true) << "\n";
    cout << "Using am/pm: "
         << time_to_string(hours, minutes, false) << "\n";

   return 0;
}

