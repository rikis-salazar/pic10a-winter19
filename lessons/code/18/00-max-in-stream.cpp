#include <string>
#include <iostream>
#include <fstream>

using namespace std;

// Reads numbers from a file and finds the maximum value. The values 0 is
// returned if the stream is empty.
double find_max_in_stream( istream& the_input_stream ){
    double highest;
    double next;

    if ( the_input_stream >> next )
        highest = next;
    else
        return 0;

    while ( the_input_stream >> next ){
        if ( next > highest )
            highest = next;
    }

    return highest;
}



// This function is slightly different from the one in the corresponding slides.
// It can read data from the console (via `std::cin`), or from a file whose name
// is provided by the user.
int main() {  
    double max;

    string input;
    cout << "Do you want to read from a file? (y/n) ";
    cin >> input;

    if ( input == "y" || input == "Y" ) {
        string the_file;
        cout << "Please enter the data file name: ";
        cin >> the_file;

        ifstream ifs;
        ifs.open( the_file.c_str() );

        if ( ifs.fail() ) {
            cout << "Error opening " << the_file << '\n';
            return 1;
        }

        max = find_max_in_stream( ifs );
        ifs.close();
    }
    else {
        max = find_max_in_stream( cin );
    }

    cout << "The maximum value is " << max << '\n';

    return 0;
}
