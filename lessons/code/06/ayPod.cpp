// Graphics classes example
//
// This program displays a _cute little old mp3 player_.
//
// > **Note:**
// >
// > This program needs the _ccc_graphics_ files provided by the author of our
// > textbook. You also need to perform additional steps in your Visual Studio
// > or Xcode setup in order to compile this example.
//
// UCLA Pic 10A Prof. Salazar


#include "ccc_win.h"    // cwin, cwin.coord(), Point(...), Circle(...),
                        // Line(...), Message(...)

int ccc_win_main() {
    // Set viewing window coordinate system
    cwin.coord(0, 0, 3, 3);

    // Create individual components
    Point p = Point(1, 1);
    Point q = Point(2, 1);
    Point r = Point(1, 2.8);
    Point s = Point(2, 2.8);
    Point t = Point(1.15, 0.9);

    Message name = Message(t,"ayPod Clasico");

    Line l1 = Line(p, q);
    Line l2 = Line(p, r);
    Line l3 = Line(r, s);
    Line l4 = Line(s, q);

    Line m1 = Line( Point(1.1, 1.1), Point(1.9, 1.1) );
    Line m2 = Line( Point(1.1, 1.1), Point(1.1, 1.8) );
    Line m3 = Line( Point(1.1, 1.8), Point(1.9, 1.8) );
    Line m4 = Line( Point(1.9, 1.8), Point(1.9, 1.1) );

    Circle c = Circle( Point(1.5, 2.30), 0.3);
    Circle d = Circle( Point(1.5, 2.30), 0.175);

    // Draw the mp3 player
    cwin << l1 << l2 << l3 << l4 << c << d << m1 << m2 << m3 << m4 << name;

    return 0;
}


/**
   Sample output: N/A
**/
