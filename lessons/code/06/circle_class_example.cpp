// `Circle` class example
//
// Using some member functions of the `Circle` class: this program displays _the
// Olympic rings_ using only one circle. The circle is moved and _displayed_ as
// needed.
//
// > **Note:**
// >
// > This program needs the _ccc_graphics_ files provided by the author of our
// > textbook. You also need to perform additional steps in your Visual Studio
// > or Xcode setup in order to compile this example.
//
// UCLA Pic 10A Prof. Salazar


#include "ccc_win.h"    // cwin, Circle(...), Circle::move(...)

int ccc_win_main(){  
    const double R = 3.0;

    // Create a circle of radius R centered at (0,R) and display it.
    Circle myCircle( Point(0,R), R );
    cwin << myCircle;

    // Move the circle R units to the right and R units down. Then display it
    // and undo the previous move. 
    myCircle.move( R, -R );
    cwin << myCircle;
    myCircle.move( -R, R );

    // Move the circle R units to the left and R units down. Then display it
    // and undo the previous move. 
    myCircle.move( -R, -R );
    cwin << myCircle;
    myCircle.move( R, R );

    // Move the circle R units to the right. Then display it and undo the
    // previous move. 
    myCircle.move( 2 * R, 0 );
    cwin << myCircle;
    myCircle.move( -2 * R, 0 );

    // Move the circle R units to the left. Then display it. No need to undo the
    // previous move (Why?).
    myCircle.move( -2 * R, 0 );
    cwin << myCircle;

    return 0;
}

/**
   Sample output: N/A
**/
