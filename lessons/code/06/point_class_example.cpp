// `Point` class example
//
// Using some member functions of the `Point` class: this program displays a
// simple plot of a quadratic function.
//
// > **Note:**
// >
// > This program needs the _ccc_graphics_ files provided by the author of our
// > textbook. You also need to perform additional steps in your Visual Studio
// > or Xcode setup in order to compile this example.
//
// UCLA Pic 10A Prof. Salazar


#include "ccc_win.h"    // cwin, Point(...), Point::move(...)

int ccc_win_main(){  
    // Creates a point at (0,0). It is not shown in the viewing window.
    Point p;

    // We want to plot the quadratic function
    //     $y = 4 - x^{2}$
    // in the range 
    //     $-3 <= x <= 3$
    // using increments of 1 unit.
    //
    // To achieve this we set the initial value of x to -3, then
    //     -   compute the value of y
    //     -   move our point p from the origin, to the coordinates (x,y)
    //     -   display the point p to the viewing window
    //     -   move the point p back to the origin
    double x = -3.0;
    double y = 4 - x * x; 
    double dx = 1.0;

    p.move( x, y );
    cwin << p;
    p.move( -x, -y );

    // Next, we increase the value of x by one unit ( x = -2 ), then
    //     -   compute the value of y
    //     -   move our point p from the origin, to the coordinates (x,y)
    //     -   display the point p to the viewing window
    //     -   move the point p back to the origin
    x += dx;
    y = 4 - x * x; 
    p.move( x, y );
    cwin << p;
    p.move( -x, -y );

    // Next, we increase the value of x by one unit ( x = -1 ), then...
    // **Note to self:** _if only there was a way to repeat this._
    x += dx;
    y = 4 - x * x; 
    p.move( x, y );
    cwin << p;
    p.move( -x, -y );

    // Next...
    x += dx;
    y = 4 - x * x; 
    p.move( x, y );
    cwin << p;
    p.move( -x, -y );

    // Next...
    x += dx;
    y = 4 - x * x; 
    p.move( x, y );
    cwin << p;
    p.move( -x, -y );

    // Next...
    x += dx;
    y = 4 - x * x; 
    p.move( x, y );
    cwin << p;
    p.move( -x, -y );

    // Next...
    x += dx;
    y = 4 - x * x; 
    p.move( x, y );
    cwin << p;
    p.move( -x, -y );

    return 0;
}


/**
   Sample output: N/A
**/
