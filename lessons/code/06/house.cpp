// Graphics classes example
//
// This program displays a _cute little house_.
//
// > **Note:**
// >
// > This program needs the _ccc_graphics_ files provided by the author of our
// > textbook. You also need to perform additional steps in your Visual Studio
// > or Xcode setup in order to compile this example.
//
// UCLA Pic 10A Prof. Salazar


#include "ccc_win.h"    // cwin, Point(...), Point::move(...), Line(...),
                        // Line::move(...)

int ccc_win_main(){  
    // Two points overlapping at the origin (not shown in viewing window).
    Point p; 
    Point q = Point(0,0);    // Same as `Point q;`

    // A vertical line...
    p.move(-5,-4);
    q.move(-5,1);
    Line l = Line(p,q);      // Same as `Line l(p,q);`
    cwin << l;

    // Translate & draw (rinse, repeat...)
    l.move(4,0);
    cwin << l;
    l.move(6,0);
    cwin << l;

    // Set new endpoints for the line and draw line at new position.
    p = Point(-4,-4);
    q = Point(-4,0);
    l = Line(p,q);
    cwin << l;

    // Translate & draw (rinse, repeat...)
    l.move(2,0);
    cwin << l;

    // Set new endpoints...
    p = Point(1,0);
    q = Point(1,-2);
    l = Line(p,q);
    cwin << l;

    // Translate & draw (rinse, repeat...)
    l.move(1,0);
    cwin << l;
    l.move(1,0);
    cwin << l;

    // Next up: the horizontal lines. Set new endpoints...
    p = Point(-5,-4);
    q = Point(5,-4);
    l = Line(p,q);
    cwin << l;

    // Set ... Translate ... Repeat as needed.
    p = Point(-4,0);
    q = Point(-2,0);
    l = Line(p,q);
    cwin << l;

    p = Point(-1,1);
    q = Point(5,1);
    l = Line(p,q);
    cwin << l;

    l.move(-1,1);
    cwin << l;
    l.move(-1,1);
    cwin << l;

    p = Point(1,-2);
    q = Point(3,-2);
    l = Line(p,q);
    cwin << l;

    l.move(0,1);
    cwin << l;
    l.move(0,1);
    cwin << l;

    // Lastly, draw the diagonal lines.
    p = Point(-5,1);
    q = Point(-3,3);
    l = Line(p,q);
    cwin << l;

    p = Point(-1,1);
    q = Point(-3,3);
    l = Line(p,q);
    cwin << l;

    l.move(6,0);
    cwin << l;

    return 0;
}


/**
   Sample output: N/A
**/
