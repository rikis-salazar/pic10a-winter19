/* ************************************************
   Ricardo Salazar
   January 27, 2015

   Example: 
   Comparing numeric types (int and double)

   Keys: 
   inexact binary repesentation, 
   inequalities vs equalities as stopping conditions.
   ************************************************ */
#include <iostream>
using namespace std;

int main() {
   int r = static_cast<int>( 4.35 * 100 );

   if ( 435 - r == 0 )
      cout << "You should be here..."; 
   else{
      cout << "... but you are here instead!" << endl; 
      cout << "Because 435 - r = " << 435 - r 
           << ". Does that make sense?\n";
   }

   return 0;
}
