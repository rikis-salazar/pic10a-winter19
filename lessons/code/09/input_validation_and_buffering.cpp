/* *****************************************************
   Ricardo Salazar
   January 30, 2014

   Example:
   A combination of input validation and buffer handling.

   Keywords: 
   infinite loops, cin.fail, buffering.
   
   ***************************************************** */
#include <iostream>
using namespace std;

int main(){
   string dummy;
   string response = "y";
   double number, sum = 0.0;

   while ( response == "y" || response == "Y" ) {
      cout << "Number: ";
      sum = 0.0;
      while ( cin >> number && number > 0 ){
	 // sum += number;
         sum = sum + number ;
	 cout << "Current sum: " << sum << "\nNumber: ";
      }
/*
   Try commenting out the next two lines
   one at a time and test the program.
   Do you understand what is going on?
   
   In the lecture slides I have the line:
   // Extra stuff needed here (see example, CCLE Lesson 9)
*/
      cin.clear();
      getline(cin,dummy);  

      cout << "Final sum: " << sum << endl;
      cout << "Continue (y/n)? ";
      cin >> response;
   }
   
   return 0;
}
