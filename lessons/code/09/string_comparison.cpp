/* ************************************************
   Ricardo Salazar
   January 27, 2015

   Example: 
   Strings and the order operators.

   Keys: Lexicographic order, dictionary order.
   numbers < UPPER CASE < lower case
   ************************************************ */

#include <iostream>
#include <string>
using namespace std;

int main(){
   string s = "chavo";
   
   if ( s =="chavo" ) 
      cout << (s == "chavo") << endl;

   if ( s =="Chavo" ) {} // not true, use else to display value
   else 
      cout << (s == "Chavo") << endl;

   if ( s < "ramon" ) 
      cout << ( s < "ramon" ) << endl;

   if ( s <"Ramon" ) ; // <-- ; used instead of {} 
   else 
      cout << (s < "Ramon") << endl;

   if ( s <"4 tortas" ) {} // not true 
   else 
      cout << (s < "4 tortas") << endl;

   return 0;
}
