/* *******************************************
   Ricardo Salazar  
   January 29, 2015
   
   Example:
   Exponential growth.

   Keywords: 
   while loop, scientific notation
   ******************************************* */
#include <iostream>
using namespace std;

int main(){
   const int MAX_POPULATION = 10e6;

   int population = 2015;
   int hours = 0;

   while ( population <= MAX_POPULATION ) {
      population *= 2;
      hours++;
      cout << "In " << hours << " hours, the population will be "
           << population << ".\n";
   }
   
   cout << "Hurry up!\n";

   return 0;
}
