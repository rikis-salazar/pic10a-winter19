/* *********************************************
   Ricardo Salazar
   February 9, 2015

   Example:
   Passing argumens by value vs 
   passing arguments by reference.

   Keywords: 
   arguments, by value, by reference.
   ********************************************* */
#include <iostream>
using namespace std; 

// 1st arg:  by value
// 2nd arg:  by reference
// 3rd arg:  by value
int moreFun(int a, int& b, int c){
    int x = 5;
    a = a + 2*b*5;
    b = b + x - 2;
    c = a*c - b;
    cout << "In moreFun a=" << a << " b=" << b 
         << " c=" << c << endl;
    return a / b;
}


int main(){
    int a = 2; 
    int x = 3;
    a = moreFun(x,x,a);
    cout << "In main a=" << a << " x=" << x << endl;

    return 0;
}
