/* *********************************************
   Ricardo Salazar
   February 9, 2015

   Example:
   Passing argumens by value vs passing arguments by reference.

   Keywords: 
   arguments, by value, by reference.
   ********************************************* */
#include <iostream>
using namespace std; 

// First argument passed by value. Second argument passed by reference
int fun(int a, int& b){
    a = 3;
    b += 2 * a;
    cout << "In fun a=" << a
         << " b=" << b << endl;
    return a - b;
}


int main(){
    int a = 1;
    int b = 3;
    a = fun(a,b);
    cout << "In main a=" << a
         << " b=" << b << endl;

    return 0;
}
