/* *********************************************
   Ricardo Salazar
   February 9, 2015

   Example:
   Passing argumens by value vs passing arguments by reference.

   Keywords: 
   arguments, by value, by reference.
   ********************************************* */
#include <iostream>
using namespace std;


/* *********************************************
   function1  
      A simple function that manipulates the values of the parameters. The
      changes persist or disappear depending on what header is the one that is
      uncommented.

   Parameters:
      a,b,c 
         three integers 

   Returns: 
      the sum of three integers.

   Testing procedure: 
      Uncomment one of the two following lines, then try to understand the
      difference in the results. If you struggle to predict the correct values,
      use your debugger.
   ********************************************* */
// int myFunction(int a, int& b, int c){
int myFunction(int a, int b, int c){
    a = 10 * a;
    b = 42;
    cout << "In myFunction a=" << a
         << " b=" << b
         << " c=" << c << endl;
    return a + b + c;
}


/* *********************************************
   main 
   ********************************************* */
int main(){
    int a = 2;
    int b = 3;
    int c = 4;

    c = myFunction(a,b,c);
    cout << "In main a=" << a
         << " b=" << b
         << " c=" << c << endl;
    return 0;
}
