/* *********************************************
   Ricardo Salazar
   February 9, 2015

   Example:
   Passing argumens by value + 
   passing arguments by reference + 
   global variables.

   Keywords: 
   arguments, by value, by reference, 
   global variables.
   ********************************************* */
#include <iostream>
#include <string>
using namespace std;


// Global variables. They can be modified everywhere.
int x;
string s;


// 1st arg: by value
// 2nd arg: by reference
int crazyFun(int a, int& b){
    s = "Cha";
    b += 3;
    x = a+b; 

    return 2*a;
}


int main (){ 
    int a = 1;
    int b = 2; 
    b = crazyFun (b,a); 
    s = s + "vo\n"; 
    cout << "a=" << a <<" b=" << b 
         << " x=" << x <<" s=" << s; 

   return 0;
}
