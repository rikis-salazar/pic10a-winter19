/* *********************************************
   Ricardo Salazar
   February 9, 2015

   Example:
   A simple recursion program. This program never ends.

   Keywords: 
   recursion, global variables, infinite loop???
   ********************************************* */

#include <iostream>
using namespace std;

int n = 1;

int main(){
    cout << "main() has been called "<< n++ << " times.\n"; 
    main();

    return 0;
}
