#ifndef RIGHT_TRIANGLE_H
#define RIGHT_TRIANGLE_H

#include "ccc_win.h"

using namespace std;


class RightTriangle {
  public:
    // ***** CONSTRUCTORS *****
    RightTriangle();
    RightTriangle( Point c );
    RightTriangle( double w, double h );
    RightTriangle( Point c, double w, double h );

    // ***** GETTERS (accessors) *****
    Point get_main_corner() const;
    double get_width() const;
    double get_height() const;
    double get_area() const;

    // ***** SETTERS (mutators) *****
    void set_main_corner( Point c );
    void set_width( double w );
    void set_height( double h );

    // ***** Miscellaneous functions *****
    void draw() const ;
    void rotate_clockwise();
    void draw_reflection() const;
    void move( double dx, double dy );
    void translate_to( double x, double y );

    // ***** Boolean operators *****
    bool operator<( const RightTriangle& rhs ) const;
    bool operator<=( const RightTriangle& rhs ) const;
    bool operator>( const RightTriangle& rhs ) const;
    bool operator>=( const RightTriangle& rhs ) const;
    bool operator==( const RightTriangle& rhs ) const;
    bool operator!=( const RightTriangle& rhs ) const;


  private:
    // ***** Auxiliary function that 'the world' does not know about *****
    void only_works_inside_RightTriangle() const;

    // The fields that represent an abstract right triangle
    Point main_corner;
    double width;
    double height;
};


void display_triangle_info( const RightTriangle &t, double y_coord );


#endif
