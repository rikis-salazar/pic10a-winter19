#include "ccc_win.h"
#include <string>

using namespace std;


// Class interface
class RightTriangle{
  public:
    RightTriangle();
    void draw() const ;

  private:
    Point main_corner;
    double width;
    double height;

  // public:
    // We do not need this field. It is determined by `width` and `height`. Let
    // us remove it.
    //
    // double area;


  public:
    // ***** OTHER CONSTRUCTORS *****
    RightTriangle( Point c );
    RightTriangle( double w, double h );
    RightTriangle( Point c, double w, double h );

    // The 4-param ctor does not make sense. Let us remove it.
    //
    //  RightTriangle( Point c, double w, double h, double a );


  public:
    // ***** ACCESSORS (`const` correct implementation) *****
    Point get_main_corner() const;
    double get_width() const;
    double get_height() const;
    double get_area() const;




  private:
    // ***** `private` can also be used for functions *****
    void only_works_inside_RightTriangle() const;


  public:
    // ***** MUTATORS (Cannot be `const`. Why?) *****
    // 
    // Setters are particular types of mutators. Their job is to provide a
    // public way to alter the value of a [private] field. They are also
    // _one-liners_ most of the time.
    void set_main_corner( Point c );
    void set_width( double w );
    void set_height( double h );
};


// Member function: Default constructor
RightTriangle::RightTriangle(){
    main_corner = Point(0,0);
    width = 2.0;
    height = 1.0;
    // area = ( width * height ) / 2;
}

// Member function: `draw()`
void RightTriangle::draw() const {
    Point aux1 = main_corner;
    Point aux2 = main_corner;

    aux1.move(width,0);
    aux2.move(0,height);

    cwin << Line( main_corner, aux1 );
    cwin << Line( aux1, aux2 );
    cwin << Line( aux2, main_corner );
}

// Member function: One-parameter constructor (constructor overload)
RightTriangle::RightTriangle( Point c ){
    main_corner = c;
    width = 2.0;
    height = 1.0;
    // area = ( width * height ) / 2;
}

// Member function: Two-parameter constructor (constructor overload)
RightTriangle::RightTriangle( double w, double h ){
    width = w;
    height = h;
    main_corner = Point(0,0);
    // area = ( width * height ) / 2;
}

// Member function: Three-parameter constructor (constructor overload)
RightTriangle::RightTriangle( Point c, double w, double h ){
    main_corner = c;
    width = w;
    height = h;
    // area = ( width * height ) / 2;
}

// Member function: Four-parameter constructor (constructor overload)
// Note: this constructor has been removed.



// ***** ACCESSORS *****
Point RightTriangle::get_main_corner() const {
    return main_corner;
}

double RightTriangle::get_width() const {
    return width;
}

double RightTriangle::get_height() const {
    return height;
}

double RightTriangle::get_area() const {
    double signed_area = ( width * height ) / 2;
    if ( signed_area >= 0 )
        return signed_area;
    else
        return -signed_area;
}




// ***** private helper functions (can be accessors or mutators) *****
void RightTriangle::only_works_inside_RightTriangle() const{
    // Draws a little circle around the main corner of a triangle
    cwin << Circle( main_corner, 0.5 );
}

// ***** MUTATORS (Cannot be `const`. Why?) *****
void RightTriangle::set_main_corner( Point c ){
    main_corner = c;
    only_works_inside_RightTriangle();
}
void RightTriangle::set_width( double w ){
    width = w;
}
void RightTriangle::set_height( double h ){
    height = h;
}




// **********************************************
// NON-MEMBER AUXILIARY FUNCTIONS
// **********************************************
void display_triangle_info( const RightTriangle& t, double y_coord ){
    cwin << Message( Point ( -9.5, y_coord ), "Info [x y w h a]:");
    cwin << Message( Point ( -4, y_coord ), t.get_main_corner().get_x() );
    cwin << Message( Point ( -1.5, y_coord ), t.get_main_corner().get_y() );
    cwin << Message( Point ( 1, y_coord ), t.get_width() );
    cwin << Message( Point ( 3.5, y_coord ), t.get_height() );
    cwin << Message( Point ( 6, y_coord ), t.get_area() );
}



// Function that tests our code.
int ccc_win_main() {
    // Draw angry face
    RightTriangle triangle1;
    triangle1.draw();

    Point center2(-5,5);
    RightTriangle triangle2( center2 );
    triangle2.draw();

    double width3 = 3.14;
    double height3 = 4.13;
    RightTriangle triangle3( width3, height3 );
    triangle3.draw();

    RightTriangle triangle4( Point(-5,-3), 8.42, -2.48 );
    triangle4.draw();

    // The area is now computed automatically
    // RightTriangle triangle5( Point(5,-3), -8.42, -2.48, -8.42 * -2.48 );
    RightTriangle triangle5( Point(5,-3), -8.42, -2.48 );
    triangle5.draw();

    center2.move(10,0);
    // Can no longer set an incorrect area.
    // RightTriangle triangle6( center2, -2, 1, -2019 );
    RightTriangle triangle6( center2, -2, 1 );
    triangle6.draw();

    // Testing accessors (indirectly via a non-member function)
    display_triangle_info( triangle1, -6 );
    display_triangle_info( triangle2, -7 );
    display_triangle_info( triangle3, -8 );
    display_triangle_info( triangle6, -9 );




    // Testing setters
    triangle1.set_main_corner( Point(-8,0) );
    triangle1.set_width( triangle1.get_height() );
    triangle1.set_height( -2 * triangle1.get_width() );
    triangle1.draw();
    display_triangle_info( triangle1, 9 );

    // The statement
    //
    // triangle1.set_main_corner( Point(8,0) );
    //
    // calls the function `only_works_inside_RightTriangle()` within the body of
    // the `set_main_corner(...)` function. If we attempt to call it here, the
    // compiler prevents us from doing so because it is a private function.
    //
    // triangle1.only_works_inside_RightTriangle();




    return 0;
}

