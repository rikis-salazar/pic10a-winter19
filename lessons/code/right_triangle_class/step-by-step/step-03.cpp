#include "ccc_win.h"
#include <string>

using namespace std;


// Class interface
class RightTriangle{
  public:
    RightTriangle();
    void draw() const;

  private:
    Point main_corner;
    double width;
    double height;

  public:
    double area;


  public:
    // ***** OTHER CONSTRUCTORS *****
    RightTriangle( Point c );
    RightTriangle( double w, double h );
    RightTriangle( Point c, double w, double h );
    RightTriangle( Point c, double w, double h, double a );


  public:
    // ***** ACCESSORS *****
    //
    // Functions whose only purpose is to "retrieve" information stored in the
    // class fields are known as _accessors_. Here we will code 4 of them, one
    // per member field.
    //
    // > Notes:
    // >
    // > 1.  Since `area` is a public field, we do not need an accessor;
    // >     however, at a later time we will remove this field while keeping
    // >     the accesor `get_area()`.
    // > 2.  So, is `const` needed? Or not?
    Point get_main_corner();
    double get_width();
    double get_height();
    double get_area();
};


// Member function: Default constructor
RightTriangle::RightTriangle(){
    main_corner = Point(0,0);
    width = 2.0;
    height = 1.0;
    area = ( width * height ) / 2;
}

// Member function: `draw()`
void RightTriangle::draw() const {
    Point aux1 = main_corner;
    Point aux2 = main_corner;

    aux1.move(width,0);
    aux2.move(0,height);

    cwin << Line( main_corner, aux1 );
    cwin << Line( aux1, aux2 );
    cwin << Line( aux2, main_corner );
}

// Member function: One-parameter constructor (constructor overload)
RightTriangle::RightTriangle( Point c ){
    main_corner = c;
    width = 2.0;
    height = 1.0;
    area = ( width * height ) / 2;
}

// Member function: Two-parameter constructor (constructor overload)
RightTriangle::RightTriangle( double w, double h ){
    width = w;
    height = h;
    main_corner = Point(0,0);
    area = ( width * height ) / 2;
}

// Member function: Three-parameter constructor (constructor overload)
RightTriangle::RightTriangle( Point c, double w, double h ){
    main_corner = c;
    width = w;
    height = h;
    area = ( width * height ) / 2;
}

// Member function: Four-parameter constructor (constructor overload)
RightTriangle::RightTriangle( Point c, double w, double h, double a ){
    main_corner = c;
    width = w;
    height = h;
    area = a;
}




// ***** ACCESSORS *****
// All of these functions are one-liners: they just return the correct value.

Point RightTriangle::get_main_corner(){
    return main_corner;
}

double RightTriangle::get_width(){
    return width;
}

double RightTriangle::get_height(){
    return height;
}

double RightTriangle::get_area(){
    return area;            // <-- `area` might not store the correct value.
}




// At this point, the interface...
int ccc_win_main() {
    // Draw angry face
    RightTriangle triangle1;
    triangle1.draw();

    Point center2(-5,5);
    RightTriangle triangle2( center2 );
    triangle2.draw();

    double width3 = 3.14;
    double height3 = 4.13;
    RightTriangle triangle3( width3, height3 );
    triangle3.draw();

    RightTriangle triangle4( Point(-5,-3), 8.42, -2.48 );
    triangle4.draw();

    RightTriangle triangle5( Point(5,-3), -8.42, -2.48, -8.42 * -2.48 );
    triangle5.draw();

    center2.move(10,0);
    RightTriangle triangle6( center2, -2, 1, -2019 );
    triangle6.draw();



    // Testing accessors
    cwin << Message( Point (-8,-6), "Triangle 1:");
    cwin << Message( Point (-4,-6), triangle1.get_main_corner().get_x() );
    cwin << Message( Point (-1.5,-6), triangle1.get_main_corner().get_y() );
    cwin << Message( Point (1,-6), triangle1.get_width() );
    cwin << Message( Point (3.5,-6), triangle1.get_height() );
    cwin << Message( Point (6,-6), triangle1.get_area() );

    cwin << Message( Point (-8,-7), "Triangle 2:");
    cwin << Message( Point (-4,-7), triangle2.get_main_corner().get_x() );
    cwin << Message( Point (-1.5,-7), triangle2.get_main_corner().get_y() );
    cwin << Message( Point (1,-7), triangle2.get_width() );
    cwin << Message( Point (3.5,-7), triangle2.get_height() );
    cwin << Message( Point (6,-7), triangle2.get_area() );

    cwin << Message( Point (-8,-8), "Triangle 3:");
    cwin << Message( Point (-4,-8), triangle3.get_main_corner().get_x() );
    cwin << Message( Point (-1.5,-8), triangle3.get_main_corner().get_y() );
    cwin << Message( Point (1,-8), triangle3.get_width() );
    cwin << Message( Point (3.5,-8), triangle3.get_height() );
    cwin << Message( Point (6,-8), triangle3.get_area() );

    cwin << Message( Point (-8,-9), "Triangle 6:");
    cwin << Message( Point (-4,-9), triangle6.get_main_corner().get_x() );
    cwin << Message( Point (-1.5,-9), triangle6.get_main_corner().get_y() );
    cwin << Message( Point (1,-9), triangle6.get_width() );
    cwin << Message( Point (3.5,-9), triangle6.get_height() );
    cwin << Message( Point (6,-9), triangle6.get_area() );

    // Note to self: a function to 'display the triangle info' might come in
    // handy at some point. We could then just write
    //
    // display_triangle_info( a_triangle, the_y_coordinate );
    // display_triangle_info( another_triangle, the_y_coordinate - 1 );
    // display_triangle_info( yet_another_triangle, the_y_coordinate - 2 );

    return 0;
}

