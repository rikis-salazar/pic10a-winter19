#include "ccc_win.h"
#include <string>

using namespace std;

// Class interface
class RightTriangle {
  public:
    RightTriangle();
    void draw() const;

  private:
    Point main_corner;
    double width;
    double height;

  public:
    // ***** OTHER CONSTRUCTORS *****
    RightTriangle( Point c );
    RightTriangle( double w, double h );
    RightTriangle( Point c, double w, double h );

  public:
    // ***** ACCESSORS (`const` correct implementation) *****
    Point get_main_corner() const;
    double get_width() const;
    double get_height() const;
    double get_area() const;

  private:
    // ***** `private` can also be used for functions *****
    void only_works_inside_RightTriangle() const;

  public:
    // ***** MUTATORS (Cannot be `const`. Why?) *****
    void set_main_corner( Point c );
    void set_width( double w );
    void set_height( double h );

  public:
    // ***** Miscellaneous functions *****
    void rotate_clockwise();
    void draw_reflection() const;
    void move( double dx, double dy );
    void translate_to( double x, double y );
    /**
    // The functions below are replaced with more "natural" operators.
    bool is_better_than( const RightTriangle& other ) const;
    bool is_equivalent_to( const RightTriangle& other ) const;
    **/




  public:
    // ***** Overloading of boolean operators *****
    //
    // Decide which triangle is "better" (the comparison is based on the areas
    // of both of the triangles being compared).
    bool operator<( const RightTriangle& rhs ) const;
    bool operator<=( const RightTriangle& rhs ) const;
    bool operator>( const RightTriangle& rhs ) const;
    bool operator>=( const RightTriangle& rhs ) const;
    // Decides whether or not two triangles are "equivalent". In this case being
    // equivalent means they both have the same area.
    bool operator==( const RightTriangle& rhs ) const;
    bool operator!=( const RightTriangle& rhs ) const;




};

// Member function: Default constructor
RightTriangle::RightTriangle(){
    main_corner = Point(0, 0);
    width = 2.0;
    height = 1.0;
}

// Member function: `draw()`
void RightTriangle::draw() const {
    Point aux1 = main_corner;
    Point aux2 = main_corner;

    aux1.move(width, 0);
    aux2.move(0, height);

    cwin << Line(main_corner, aux1);
    cwin << Line(aux1, aux2);
    cwin << Line(aux2, main_corner);
}


// Member function: One-parameter constructor (constructor overload)
RightTriangle::RightTriangle( Point c ){
    main_corner = c;
    width = 2.0;
    height = 1.0;
}

// Member function: Two-parameter constructor (constructor overload)
RightTriangle::RightTriangle( double w, double h ){
    main_corner = Point(0, 0);
    width = w;
    height = h;
}

// Member function: Three-parameter constructor (constructor overload)
RightTriangle::RightTriangle(Point c, double w, double h){
    main_corner = c;
    width = w;
    height = h;
}


// ***** ACCESSORS *****
Point RightTriangle::get_main_corner() const {
    return main_corner;
}

double RightTriangle::get_width() const {
    return width;
}

double RightTriangle::get_height() const {
    return height;
}

double RightTriangle::get_area() const {
    double signed_area = (width * height) / 2;
    if (signed_area >= 0)
        return signed_area;
    else
        return -signed_area;
}


// ***** private helper functions (can be accessors or mutators) *****
void RightTriangle::only_works_inside_RightTriangle() const {
    // Draws a little circle around the main corner of a triangle
    cwin << Circle(main_corner, 0.5);
}


// ***** MUTATORS *****
void RightTriangle::set_main_corner( Point c ){
    main_corner = c;
    only_works_inside_RightTriangle();
}
void RightTriangle::set_width( double w ){
    width = w;
}
void RightTriangle::set_height( double h ){
    height = h;
}


// ***** MISCELLANEOUS FUNCTIONS *****
//
// Alters width and height but does not redraw the triangle.
void RightTriangle::rotate_clockwise(){
    // Clockwise rotation:
    //     (x,y) --> (y,-x)
    double x = width;
    double y = height;
    width = y;
    height = -x;
}

// Draws a reflected triangle, but the original triangle remains unchanged.
void RightTriangle::draw_reflection() const {
    // Reflection w.r.t origin:
    //     (x,y) --> (-x,-y)
    double x = width;
    double y = height;
    RightTriangle reflected_local_triangle(main_corner, -x, -y);
    reflected_local_triangle.draw();
}

// Moves the triangle by the specified increments.
void RightTriangle::move( double dx, double dy ){
    main_corner.move(dx, dy);
}

// Moves the triangle to its new location.
void RightTriangle::translate_to( double x, double y ){
    main_corner = Point(x, y);
}

/**
// DEPRECATED FUNCTIONS (i.e., they are no longer supported and will be removed)
bool RightTriangle::is_better_than( const RightTriangle& other ) const {
    if ( get_area() > other.get_area() )
        return true;
    else
        return false;
}

bool RightTriangle::is_equivalent_to( const RightTriangle& other ) const {
    return get_area() == other.get_area();
}
**/




// ***** Overloading of boolean operators *****
//
// Comparisons based on areas.
bool RightTriangle::operator<( const RightTriangle& rhs ) const {
    return get_area() < rhs.get_area();
}

bool RightTriangle::operator<=( const RightTriangle& rhs ) const {
    return get_area() <= rhs.get_area();
}

bool RightTriangle::operator>( const RightTriangle& rhs ) const {
    return get_area() > rhs.get_area();
}

bool RightTriangle::operator>=( const RightTriangle& rhs ) const {
    return get_area() >= rhs.get_area();
}

bool RightTriangle::operator==( const RightTriangle& rhs ) const {
    return get_area() == rhs.get_area();
}

bool RightTriangle::operator!=( const RightTriangle& rhs ) const {
    return get_area() != rhs.get_area();
}




// **********************************************
// NON-MEMBER AUXILIARY FUNCTIONS
// **********************************************
void display_triangle_info( const RightTriangle &t, double y_coord ) {
    cwin << Message(Point(-9.5, y_coord), "Info [x y w h a]:");
    cwin << Message(Point(-4, y_coord), t.get_main_corner().get_x());
    cwin << Message(Point(-1.5, y_coord), t.get_main_corner().get_y());
    cwin << Message(Point(1, y_coord), t.get_width());
    cwin << Message(Point(3.5, y_coord), t.get_height());
    cwin << Message(Point(6, y_coord), t.get_area());
}

// Function that tests our code.
int ccc_win_main() {
    // Draw angry face
    RightTriangle triangle1;
    triangle1.draw();

    Point center2(-5, 5);
    RightTriangle triangle2(center2);
    triangle2.draw();

    double width3 = 3.14;
    double height3 = 4.13;
    RightTriangle triangle3(width3, height3);
    triangle3.draw();

    RightTriangle triangle4(Point(-5, -3), 8.42, -2.48);
    triangle4.draw();

    RightTriangle triangle5(Point(5, -3), -8.42, -2.48);
    triangle5.draw();

    center2.move(10, 0);
    RightTriangle triangle6(center2, -2, 1);
    triangle6.draw();

    // Testing accessors (indirectly via a non-member function)
    display_triangle_info(triangle1, -6);
    display_triangle_info(triangle2, -7);
    display_triangle_info(triangle3, -8);
    display_triangle_info(triangle6, -9);

    // Testing setters
    triangle1.set_main_corner( Point(-8, 0) );
    triangle1.set_width( triangle1.get_height() );
    triangle1.set_height( -2 * triangle1.get_width() );
    triangle1.draw();
    display_triangle_info(triangle1, 9);

    // Testing miscellaneous functions
    triangle1.draw_reflection();

    triangle1.translate_to(8,0);
    triangle1.draw();

    for ( int i = 0 ; i < 3 ; i++ ){
        triangle1.rotate_clockwise();
        triangle1.draw();
    }

    triangle1.move(-10,0);
    triangle1.draw();



    // Testing boolean operators
    Point p = triangle3.get_main_corner();
    p.move(-6,-1);
    // if ( triangle3.is_better_than(triangle2) )
    if ( triangle3 >= triangle2 )
        cwin << Message( p, "Triangle 3 is bigger than triangle 2" );

    p.move(0, -1);
    // if ( !triangle4.is_equivalent_to(triangle6) )
    if ( triangle4 != triangle6 )
        cwin << Message( p, "Triangles 4 & 6 have different areas" );



    return 0;
}
