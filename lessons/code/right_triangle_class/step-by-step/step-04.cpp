#include "ccc_win.h"
#include <string>

using namespace std;


// Class interface
class RightTriangle{
  public:
    RightTriangle();
    void draw() const;

  private:
    Point main_corner;
    double width;
    double height;

  public:
    double area;


  public:
    // ***** OTHER CONSTRUCTORS *****
    RightTriangle( Point c );
    RightTriangle( double w, double h );
    RightTriangle( Point c, double w, double h );
    RightTriangle( Point c, double w, double h, double a );


  public:
    // ***** ACCESSORS (a non `const` correct implementation) *****
    Point get_main_corner();
    double get_width();
    double get_height();
    double get_area();

/**
    // ***** ACCESSORS (a `const` correct implementation) *****
    Point get_main_corner() const;
    double get_width() const;
    double get_height() const;
    double get_area() const;
**/
};


// Member function: Default constructor
RightTriangle::RightTriangle(){
    main_corner = Point(0,0);
    width = 2.0;
    height = 1.0;
    area = ( width * height ) / 2;
}

// Member function: `draw()`
void RightTriangle::draw() const {
    Point aux1 = main_corner;
    Point aux2 = main_corner;

    aux1.move(width,0);
    aux2.move(0,height);

    cwin << Line( main_corner, aux1 );
    cwin << Line( aux1, aux2 );
    cwin << Line( aux2, main_corner );
}

// Member function: One-parameter constructor (constructor overload)
RightTriangle::RightTriangle( Point c ){
    main_corner = c;
    width = 2.0;
    height = 1.0;
    area = ( width * height ) / 2;
}

// Member function: Two-parameter constructor (constructor overload)
RightTriangle::RightTriangle( double w, double h ){
    width = w;
    height = h;
    main_corner = Point(0,0);
    area = ( width * height ) / 2;
}

// Member function: Three-parameter constructor (constructor overload)
RightTriangle::RightTriangle( Point c, double w, double h ){
    main_corner = c;
    width = w;
    height = h;
    area = ( width * height ) / 2;
}

// Member function: Four-parameter constructor (constructor overload)
RightTriangle::RightTriangle( Point c, double w, double h, double a ){
    main_corner = c;
    width = w;
    height = h;
    area = a;
}




// ***** ACCESSORS (non `const` correct implementation) *****
Point RightTriangle::get_main_corner(){
    return main_corner;
}
double RightTriangle::get_width(){
    return width;
}
double RightTriangle::get_height(){
    return height;
}
double RightTriangle::get_area(){
    return area;            // <-- `area` might not store the correct value.
}


/**
// ***** ACCESSORS (`const` correct implementation) *****
Point RightTriangle::get_main_corner() const {
    return main_corner;
}
double RightTriangle::get_width() const {
    return width;
}
double RightTriangle::get_height() const {
    return height;
}
double RightTriangle::get_area() const {
    // return area; // Returns value stored in field instead of actual area.
    double signed_area = ( width * height ) / 2;
    if ( signed_area >= 0 )
        return signed_area;
    else
        return -signed_area;
}
**/




// **********************************************
// NON-MEMBER AUXILIARY FUNCTIONS
// **********************************************
void display_triangle_info( RightTriangle t, double y_coord ){
    cwin << Message( Point ( -9.5, y_coord ), "Info [x y w h a]:");
    cwin << Message( Point ( -4, y_coord ), t.get_main_corner().get_x() );
    cwin << Message( Point ( -1.5, y_coord ), t.get_main_corner().get_y() );
    cwin << Message( Point ( 1, y_coord ), t.get_width() );
    cwin << Message( Point ( 3.5, y_coord ), t.get_height() );
    cwin << Message( Point ( 6, y_coord ), t.get_area() );

    t.area = 0.0; // <-- A harmless statement. Why?
}
// Note: 
//
// The parameter `t` (a triangle) is passed by value. This means that a copy of
// this object is made. This might be "costly". An alternative calls for a
// reference parameter... but it might not be safe (see last statement in the
// previous function).
//
// To avoid an unnecessary copy of the triangle while still preventing the
// reference parameter to change, we can add extra insurance via the `const`
// modifier. The signature of the function needs to be changed to
//
//     void display_triangle_info( const Triangle& t, double y_coord )
//
// However this change has consequences...

/**
void display_triangle_info( const RightTriangle& t, double y_coord ){
    cwin << Message( Point ( -9.5, y_coord ), "Info [x y w h a]:");
    cwin << Message( Point ( -4, y_coord ), t.get_main_corner().get_x() );
    cwin << Message( Point ( -1.5, y_coord ), t.get_main_corner().get_y() );
    cwin << Message( Point ( 1, y_coord ), t.get_width() );
    cwin << Message( Point ( 3.5, y_coord ), t.get_height() );
    cwin << Message( Point ( 6, y_coord ), t.get_area() );

    // t.area = 0.0; // <-- Here the compiler "spots" the faulty code.
}
**/



// Function that tests our code.
int ccc_win_main() {
    // Draw angry face
    RightTriangle triangle1;
    triangle1.draw();

    Point center2(-5,5);
    RightTriangle triangle2( center2 );
    triangle2.draw();

    double width3 = 3.14;
    double height3 = 4.13;
    RightTriangle triangle3( width3, height3 );
    triangle3.draw();

    RightTriangle triangle4( Point(-5,-3), 8.42, -2.48 );
    triangle4.draw();

    RightTriangle triangle5( Point(5,-3), -8.42, -2.48, -8.42 * -2.48 );
    triangle5.draw();

    center2.move(10,0);
    RightTriangle triangle6( center2, -2, 1, -2019 );
    triangle6.draw();



    // Testing accessors (indirectly via a non-member function)
    display_triangle_info( triangle1, -6 );
    display_triangle_info( triangle2, -7 );
    display_triangle_info( triangle3, -8 );
    display_triangle_info( triangle6, -9 );

    display_triangle_info( triangle1, 9 );
    // The area of triangle 1 is not set to 0 after all. Why?


    return 0;
}

