// Graphics classes example
//
// Another example of input handling via mouse clicks. The user uses clicks to
// draw a curve (poly-line?!) in the plane.
//
// > **Note:**
// >
// > This program needs the _ccc_graphics_ files provided by the author of our
// > textbook. You also need to perform additional steps in your Visual Studio
// > or Xcode setup in order to compile this example.
//
// UCLA Pic 10A Prof. Salazar


#include "ccc_win.h"    // cwin, Line, Message, etc.

#include "ccc_win.h"

int ccc_win_main(){
    // Axes (set up) 
    cwin.coord (0,11,11,0);
    cwin << Line ( Point(0,1) , Point(11,1) );
    cwin << Line ( Point(1,0) , Point(1,10) );

    // Labels  (y-axis)
    cwin << Message( Point(0.5,10) , "10" );
    cwin << Message( Point(0.5,8) , "8" );
    cwin << Message( Point(0.5,6) , "6" );
    cwin << Message( Point(0.5,4) , "4" );
    cwin << Message( Point(0.5,2) , "2" );

    // Labels (x-axis)
    cwin << Message( Point(2,1) , "1" );
    cwin << Message( Point(4,1) , "2" );
    cwin << Message( Point(6,1) , "3" );
    cwin << Message( Point(8,1) , "4" );
    cwin << Message( Point(10,1) , "5" );

    // The message below is not displayed. Why?
    // cwin << Message( Point(0,11), "Sketch your own function" );

    // On the other hand, this one is displayed. Why?
    cwin << Message( Point(0,10.5), "Sketch your own function." );

    // Get mouse clicks and connect dots _on the fly_.
    Point p1 = cwin.get_mouse("Click the first point.");
    cwin << p1;
    Point p2 = cwin.get_mouse("Click the second point.");
    cwin << p2 << Line (p1, p2);
    Point p3 = cwin.get_mouse("Click the third point.");
    cwin << p3 << Line (p2, p3);
    Point p4 = cwin.get_mouse("Click the fourth point.");
    cwin << p4 << Line (p3, p4);
    Point p5 = cwin.get_mouse ("Click the fifth point.");
    cwin << p5 << Line (p4, p5);

    return 0;
}

/**
   Sample output: N/A
**/
