// Variables, input & buffering: 
//
// Reads 4 integer values corresponding to pennies (1 cent), nickels (5 cents),
// dimes (10 cents), and quarters (25 cents); then displays the total amount of
// money in dollars.
//
// UCLA Pic 10A
// Prof. Salazar

#include <iostream>     // std::cout, std::cin

using namespace std;

int main() {
    // Read quantities from user
    int pennies, nickels, dimes, quarters;
    cout << "How many pennies do you have? ";
    cin >> pennies;
    cout << "How many nickels do you have? ";
    cin >> nickels;
    cout << "How many dimes do you have? ";
    cin >> dimes;
    cout << "How many quarters do you have? ";
    cin >> quarters;

    // Compute total value in dollars and display to console
    double total = pennies * 0.01 + nickels * 0.05 + dimes * 0.10
                 + quarters * 0.25;
    cout << "The total value of your coins is " << total << " dollars.\n";

    return 0;
}

/**
   Sample output:
  
       How many pennies do you have? 5
       How many nickels do you have? 8
       How many dimes do you have? 2
       How many quarters do you have? 3
       The total value of your coins is 1.4 dollars.
**/
