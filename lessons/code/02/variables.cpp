// Variables: use names that describe the values they hold.
// UCLA Pic 10A
// Prof. Salazar


#include <iostream>     // std::cout

using namespace std;

int main() {
    int pennies = 8;
    int dimes = 4;
    int quarters = 3;

    double total = pennies * 0.01 + dimes * 0.10 + quarters * 0.25;

    cout << "Total value = " << total << '\n';

    return 0;
}

/**
   OUTPUT:
  
       Total value = 1.23
**/
