// Logic error: _garbage_ output.
// UCLA Pic 10A
// Prof. Salazar


#include <iostream>     // std::cout

using namespace std;

int main() {
    int number;

    cout << "N = " << number << '\n';

    return 0;
}

/**
   OUTPUT:
  
       N = [Some random _garbage_ integer]
**/
