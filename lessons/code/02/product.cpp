// Variables & input: computing the product of two integers.
// UCLA Pic 10A
// Prof. Salazar


#include <iostream>     // std::cout, std::cin

using namespace std;

int main() {
    
    cout << "Type two (whole) numbers: ";

    int num1, num2;
    cin >> num1 >> num2;

    cout << "The product is " << num1 * num2 << ".\n";

    return 0;
}

/**
   Sample output:
  
       Type two (whole) numbers: 3 5
       The product is 15.
**/
