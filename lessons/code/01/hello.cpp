// A basic 'hello world' program.
// UCLA Pic 10A
// Prof. Salazar


#include <iostream>     // std::cout

using namespace std;

int main() {
    cout << "Hola, mundo.\n";
    return 0;
}

/**
   OUTPUT:
  
       Hola, mundo.
**/
