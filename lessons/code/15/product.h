/* *********************************************
   Ricardo Salazar
   February 16, 2015

   Example:
      Product class: interface

   Keywords: 
      Separate compilation, function overloading
      operator overloading
   ********************************************* */

#ifndef PRODUCT_H
#define PRODUCT_H


#include <iostream>
#include <string>
using namespace std;    // <-- This line is actually dangerous. We'll explain


// Product interface
class Product{
  public:
    // Contructors
    Product();
    Product( string n, double p, int s );

    // Mutator
    void read();

    // Accessors
    void print() const;
    bool operator>( Product b ) const ;

  private:
    string name;
    double price;
    int score;
};

#endif
