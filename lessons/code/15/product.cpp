/* *********************************************
   Ricardo Salazar
   February 16, 2015

   Example:
      Product class: Implementation of functions

   Keywords: 
      see product.h for keywords
   ********************************************* */


#include "product.h" 


// Default constructor 
Product::Product(){
    name = "No item";
    price = 1.0;
    score = 0;
}


// Overloaded constructor: Creates a product with the information specified in
// the parameters.
Product::Product( string n, double p, int s ){
    name = n;
    price = p;
    score = s;
}


// Prompts user for a string (name), a double (price), and an integer (score),
// then sets the private fields to those values provided by the user.
// Note: a dummy variable is used to clear the buffer (cin).
void Product::read(){
    cout << "Please enter the model name: ";
    getline(cin, name);
    cout << "Please enter the price: ";
    cin >> price;
    cout << "Please enter the score: ";
    cin >> score;
    string remainder;
    getline(cin, remainder);
    return;
}


// Displays the contents of the private fields.
void Product::print() const {
    cout << "Name: " << name << endl;
    cout << "Price: " << price << endl;
    cout << "Score: " << score << endl;
    return;
}


// Operator that compares two Products based on the ratio score/price. Note:
// this operator replaces the `is_better_than(...)` function coded earlier.
bool Product::operator > (Product b) const {
    if ( b.price == 0 )
        return false;
    if ( price == 0 )
        return true;
   
   return score/price > b.score/b.price;
}
