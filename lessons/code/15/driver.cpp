/* *********************************************
   Ricardo Salazar
   February 16, 2015

   Example:
      Another driver for the Product class

   Keywords: 
      three-file-layout, overloaded functions,
      overloaded operators
   ********************************************* */

#include "product.h"

int main(){
    Product next; 

    // Create initial object
    Product best = Product("El chavo del 8", 0.00, 10);

    cout << "Can you beat the following product?" << endl;
    best.print();

    next.read();
    if ( next > best ){
        best = next;
    }

    cout << "The best product is:" << endl;
    best.print();
    cout << "Sorry. Better luck next time!" << endl;

    return 0;
}
