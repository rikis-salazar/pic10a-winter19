---
author: Ricardo Salazar
title: Streams (Chapter 9) 
date: March 13, 2019
---

# Introduction

## Goals

To be able to

. . . 

*   read data from files

*   write data to files

*   learn about _stream manipulators_ to format output

*   convert between strings and numbers using _string streams_


## Types of Streams

::: nonincremental

The `C++` input/output library is based on special objects called _streams_:

:::::: incremental

. . .

*   _input stream_ -- source of data (_e.g.,_ `std::cin`)

*   _output stream_ -- destination for data (_e.g.,_ `std::cout`)

*   _string stream_ -- allows to manipulate contents of strings

::::::

. . .

Special classes (called _wrappers_) provide stream access to

:::::: incremental

. . .

-   files (including the screen and keyboard)

-   strings

::::::

:::

# File streams

## (9.1) Reading and writing text files

*   Include the header `<fstream>`

*   Use a file variable to access a disk file:

    -   `std::ifstream` -- for reading (input)

    -   `std::ofstream` -- for writing (output)

    -   `std::fstream` -- for both reading and writing

*   Example:  

    ~~~~~ {.cpp}  
    #include <fstream>    // std::ifstream
    using namespace std;  
    int main(){
        ifstream read_from_file;  
        ...  
    }  
    ~~~~~  


## Reading data from files (input)

*   The file must be _opened_ before data can be read:

    ~~~~~ {.cpp}  
    read_from_file.open("file_with_data.txt");  
    ~~~~~   

*   `read_from_file` can now read data from the _opened_ file:

    ~~~~~ {.cpp}
    int n;
    double x;
    read_from_file >> n >> x;   // much like `std::cin`
    ~~~~~

*   Strings are read the usual way:

    ~~~~~ {.cpp}
    string s;
    read_from_file >> s;        // Reads a word 
    getline(read_from_file, s); // Reads a line
    ~~~~~

## Processing `char` values

*  Use the `get` member function to read a single character:

    ~~~~~ {.cpp}
    char ch;
    read_from_file.get(ch);
    ~~~~~

*   Use unget[^two] for a one-character lookahead:

    ~~~~~ {.cpp}
    char ch;
    read_from_file.get(ch);
    if ( '0' <= ch && ch <= '9' ) { // Begining of a number.
        read_from_file.unget();     // First digit captured,
        int n;                      // need to put it back &
        read_from_file >> n;        // read whole number. 
    }
    ~~~~~

[^two]: Older versions of `<fstream>` use `put_back`:
  `read_from_file.put_back(ch);`


## \emph{fail}ures, lockups, and data corruption 

*   Streams may be in a `fail` state if:

    -   the associated file could not be opened (_e.g.,_ doesn't exist)
    
    -   the set of permissions on a file does not allow you to access it
    
    -   the stream has reached the end of an input file

*   The `fail` function tells you when input has failed

*   After processing a file, close the associated stream:

    ~~~~~ {.cpp}
    read_from_file.close();
    ~~~~~

    -   This prevents data corruption and/or locking up resources

## Writing to files (output)

*   Writing to a file is similar

*   Use an `ofstream` object:

    ~~~~~ {.cpp}
    ofstream write_to_file;
    write_to_file.open("output.dat");
    ~~~~~

*   Send information:

    ~~~~~ {.cpp}
    write_to_file << n << " " << x;  // just like std::cout
    ~~~~~

*   Write a single character:

    ~~~~~ {.cpp}
    write_to_file.put(ch);
    ~~~~~

*   Remember to close the stream:

    ~~~~~ {.cpp}
    write_to_file.close();
    ~~~~~

## Some technicalities

*   In older versions of `<fstream>`, the member function `open` expects a
    C-style string instead of a C++ string:

    ~~~~~ {.cpp}
    ofstream write_to_file;
    write_to_file.open("input.dat"); // OK
    // C-style string   ^^^^^^^^^ 
    ~~~~~

*   If needed, use the member function `c_str()`:

    ~~~~~ {.cpp}
    string input_name;  // C++ string.
    cin >> input_name;
    ofstream write_to_file;
    // This might not work:
    //     write_to_file.open( input_name );
    write_to_file.open( input_name.c_str() );  // OK
    ~~~~~

## Some technicalities (cont)

*   Filenames may contain path information:

    ~~~~~ {.cpp}
    // In Unix, Linux, Android, Mac OS
    string file = "~/docs/data.txt"; 
    ofstream write_to_file;
    write_to_file.open( file.c_str() );
    ~~~~~

*   Backslashes (used in Windows) need to be escaped:

    ~~~~~ {.cpp}
    string file = "c:\\docs\\data.txt";
    ofstream write_to_file;
    write_to_file.open( file.c_str() );
    ~~~~~

## _Streams_ as parameters

_Streams_ **cannot be copied**. Use reference parameters.

~~~~~ {.cpp}
double find_max_in_stream( ifstream& the_input_stream );
int main() {
    string the_file = "/home/user/some_file.txt";
    ifstream ifs;  // ifs = input file stream
    ifs.open( the_file.c_str() );
    if ( ifs.fail() ) {
      return 1;    // Error encountered. Terminate program.
    }
    double max = find_max_in_stream( ifs );
    cout << "The maximum value is " << max << '\n';
    ifs.close();
    return 0;
}
~~~~~

## _Streams_ as parameters (cont)

~~~~~ {.cpp}
double find_max_in_stream( ifstream& the_input_stream ) {
    double highest;
    double next;
    if ( the_input_stream >> next )
        highest = next;
    else
        return 0;

    while ( the_input_stream >> next )
        if ( next > highest )
            highest = next;

    return highest;
}
~~~~~

# Manipulators

## (9.3) Stream manipulators

*   To access stream manipulators include `<iomanip>`. They

    -   allow you to format output streams

    -   are sent to a stream via the _shift left_ operator `<<`

*   The function `setprecision()` is an example:

    ~~~~~ {.cpp}
    output_stream << setprecision(2);
    std::cout << setprecision(4);
    ~~~~~

    -   Recall that
    
        +   characters (trailing zeros) are not added[^one]

        +   subsequent float-type outputs are affected by this statement

[^one]: Use the `fixed` manipulator to add trailing zeros.


## Common manipulators

*   `setw` -- sets field with of next item
*   `setfill` -- sets fill (padding) character; defaults to blank space
*   `left` -- left alignment
*   `right` -- right alignment  
*   `hex` -- integers displayed in base 16

. . .

| **Statement** | **Output** |  
|:--------------------|:----------|  
| `// _ denotes a blank space ` | |  
| `out << setw(6) << 123;` | `___123` |  
| `out << setfill('*') << setw(6) << 123;` | `***123` |  
| `out << left << setw(6) << 123;` | `123___` |  
| `out << right << setw(6) << 123;` | `___123` |  
| `out << hex << 64206;` | `face` |  


## Common manipulators (cont)

*   `dec` -- integers displayed in base 10
*   `fixed` -- fixed format for _floating-point_ numbers
*   `setprecision` -- sets number of
    -   significant digits for _general_ format
    -   digits after decimal point for _fixed_ format

. . .

| **Statement** | **Output** |  
|:--------------------|:----------|  
| `// 0x indicates hexadecimal number` | |  
| `out << dec << 0xc0ffee;` | `12648430` |  
| `out << 123.45;` | `123.45` |  
| `out << fixed << 123.45;` | `123.450000` |  
| `out << setprecision(2) << 987.6;` | `9.9e+02` |  
| `out << setprecision(2) << fixed << 987.6;` | `987.60` |  


## _general_ vs _fixed_

*   Default for float-type is _general_ format

    -   Displays number of digits set with `setprecision` (default is 6)
    -   Switches to _scientific_ as needed  

    > ~~~~~ {.cpp}
    > out << 12.3456789 << ' ' << 123456789.0;
    > // Output: 12.3457 1.23457e+08
    > ~~~~~

*   `fixed` uses same number of digits after the decimal  

    > ~~~~~ {.cpp}
    > out << fixed << 12.3456789 << ' ' << 123456789.0;  
    > // Output: 12.345679 123456789.000000
    > ~~~~~

*   Manipulators set the state for all subsequent operations

    -   Except `setw`,  it resets to 0 every time


# `string` streams 

## (9.4) `string` streams

*   Use the `<sstream>` header file

*   Used to _"read from"_ and _"write to"_ strings (like other files)

*   Handy for 

    - reading a number in a string
    
    - converting numbers into strings

*   `istringstream` objects read data from a string

*   `ostringstream` objects write data to a string

## `istringstream` -- input string streams

*   Problem: Given a date, extract the month, day, and year

    ~~~~~ {.cpp}
    string independence_day = "September 16, 1810";
    // Create stream associated to input string
    istringstream input_stream(independence_day);
    ~~~~~

*   Can now use `>>` to read the month (`string`), day (`int`), comma (`string`),
    and year (`int`):

    ~~~~~ {.cpp}
    string month;
    string comma;
    int day, year;
    input_stream >> month >> day >> comma >> year;
    ~~~~~

*   Had we used `substr()` to take this apart, we'd have only strings, which we
    would need to convert to integers


## `istringstream` (cont)

*   Converting strings to integers is common

*   A helper function in our private library might be handy:

    ~~~~~ {.cpp}
    int string_to_int(string s) {
        istringstream input_stream(s);
        int n;
        input_stream >> n;
        return n;
    }
    ~~~~~

*   Can be used like this:

    ~~~~~ {.cpp}
    int i = string_to_int("2019");
    ~~~~~


## `ostringstream` -- output string streams

*   To convert a number to a string:

*   Construct an `ostringstream` object:

    ~~~~~ {.cpp}
    ostringstream output_stream;
    ~~~~~

*   Use `<<` to send a number to the stream:

    ~~~~~ {.cpp}
    output_stream << setprecision(5) << sqrt(2);
    ~~~~~

*   Use the `str()` function to recover the string:

    ~~~~~ {.cpp}
    string output = output_stream.str();
    ~~~~~

## `ostringstream` (cont)

*   Example: (builds the string "September 16, 1810")

    ~~~~~ {.cpp}
    string month = "September";
    int day = 16;
    int year = 1810;
    ostringstream output_stream;
    output_stream << month << " " << day << ", " << year;
    string output = output_stream.str();
    ~~~~~

*   Another useful helper function:

    ~~~~~ {.cpp}
    string int_to_string(int n) {
        ostringstream output_stream;
        output_stream << n;
        return output_stream.str();
    }
    ~~~~~

## Example: reading time

*   We commonly accept one line at a time, then _parse[^six]_ each line

*   String streams make this task easier

*   Avoids problems of mixing `<<` and `getline()`

*   Problem: interpret time provided by user, where input lines might be

    ~~~~~
    21:30
    9:30 pm
    9 am
    ~~~~~

*   Inputs are converted to 2 integers: _hh_, and _mm_

*   A new _"standard"_ string is created: _hh:mm_


[^six]: _parse_.- analyze (a sentence) into its parts and describe their syntactic roles.


## Example: reading time (cont)

The `read_time` function accepts entire line and computes time

. . .

~~~~~ {.cpp}
void read_time( int& hours, int& minutes ) {  
    // Read whole line into string `input`
    // Create string stream `iss` to process `input`
    // Read integer `hours` from `iss`
    // Read next character `ch` in `iss`
    // If user entered minutes (i.e., ':' char is found)
    //     read integer `minutes` from `iss`
    // Else
    //     Set `minutes` to 0
    //     Put `ch` back in stream, might be 'a' or 'p'
    // Read string `suffix` in `iss`
    // If "pm"
    //     adjust `hours`
}
~~~~~


## Example: reading time (cont)

~~~~~ {.cpp}
void read_time( int& hours, int& minutes ) {  
    string line;
    getline(cin, line);
    istringstream iss( line.c_str() );
    iss >> hours;

    char ch;
    iss.get(ch);
    if (ch == ':') iss >> minutes;
    else { minutes = 0; iss.unget(); }

    string suffix; iss >> suffix;
    if ( suffix == "pm" ) hours += 12;
}

~~~~~

## Example: reading time (cont)

The `time_to_string` function creates the new _"standard"_ time

. . .

~~~~~ {.cpp}
string time_to_string(int hours, int minutes, bool military){
    // Determine `suffix` ("am"/"pm") based on parameters
    // Convert `hours` (int) to string, add to `result`  (*)
    // Add colon to `result` (if needed)
    // If `minutes` < 10
    //     Add '0' to `result`
    // Process `minutes` (int), add to `result`          (*)
    // Add `suffix` to `result`
    // Standar time is ready (`result`), send it back
}
// (*) See funtion `int_to_string(...)` from previous slides
~~~~~


## Example: reading time (cont)

~~~~~ {.cpp}
string time_to_string(int hours, int minutes, bool military){
    // Determine suffix (if needed) and correct hours value
    string suffix;
    if ( !military ) {
        if ( hours < 12 ) suffix = "am";
        else { suffix = "pm"; hours = hours - 12; }
        if ( hours == 0 ) hours = 12;
    } // Next process hours and add ":0" (if needed)
    string result = int_to_string(hours) + ":";
    if ( minutes < 10 ) result = result + "0";
    // Process minutes
    result = result + int_to_string(minutes);
    if ( !military ) result = result + " " + suffix;
    return result;   // DONE!
} // Code accessible at http://cpp.sh/3cxzcn
~~~~~


## Other resources (clickable)

::: nonincremental

*   [Old Google slides presentation: File Input/Output][old-lesson18]

*   [Practice code][c18]

    -   [`00-max-in-stream.cpp`][ex00]
    -   [`01-manipulators.cpp`][ex01]
    -   [`02-read-time.cpp`][ex02]

*   [Pic 10b handout: Figuring out your _working directory_][h1]

:::

[old-lesson18]: https://docs.google.com/presentation/d/e/2PACX-1vSTGWhSS0hQN2-cr2bL5nPptzRtFP9e4bV6X1F-WfHWjGLalz0eqxZnYBLpkyBy9TeUk4ODOaVV8ELV/pub?start=false&loop=false&delayms=3000
[c18]: https://www.pic.ucla.edu/~rsalazar/pic10a/lessons/code/18
[ex00]: https://www.pic.ucla.edu/~rsalazar/pic10a/lessons/code/18/00-max-in-stream.cpp
[ex01]: https://www.pic.ucla.edu/~rsalazar/pic10a/lessons/code/18/01-manipulators.cpp
[ex02]: https://www.pic.ucla.edu/~rsalazar/pic10a/lessons/code/18/02-read-time.cpp
[h1]: https://www.pic.ucla.edu/~rsalazar/pic10b/handouts/data-files-vs.html


